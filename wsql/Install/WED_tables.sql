--multiples instance of a particular wedflow

DROP TABLE IF EXISTS WED_attr;
DROP TABLE IF EXISTS WED_trace;
DROP TABLE IF EXISTS JOB_POOL;
DROP TABLE IF EXISTS WED_trig;
DROP TABLE IF EXISTS WED_cond;
DROP TABLE IF EXISTS WED_trans;
DROP TABLE IF EXISTS ST_STATUS;
DROP TABLE IF EXISTS WED_flow CASCADE;

-- An '*' means that WED-attributes columns will be added dynamicaly after an INSERT on WED-attr table
--*WED-flow instances
CREATE TABLE WED_flow (
    wid     SERIAL PRIMARY KEY
);

CREATE TABLE WED_attr (
    aid     SERIAL PRIMARY KEY,
    aname    TEXT NOT NULL,
    adv   TEXT
);
-- name must be unique 
CREATE UNIQUE INDEX wed_attr_lower_name_idx ON WED_attr (lower(aname));

CREATE TABLE WED_cond (
    cid     SERIAL PRIMARY KEY,
    cname   TEXT NOT NULL,
    cpred   TEXT NOT NULL 
);
CREATE UNIQUE INDEX wed_cond_lower_name_idx ON WED_cond (lower(cname));

CREATE TABLE WED_trans (
    trid     SERIAL PRIMARY KEY,
    trname   TEXT NOT NULL,
    trtout   INTERVAL DEFAULT '1Y',
    trsc     TEXT DEFAULT NULL,
    trsp     BOOL DEFAULT FALSE
);
CREATE UNIQUE INDEX wed_trans_lower_name_idx ON WED_trans (lower(trname));

--this table must be written only by SP !
CREATE TABLE WED_trig (
    tgid     SERIAL PRIMARY KEY,
    tgname  TEXT NOT NULL DEFAULT '',
    enabled BOOL NOT NULL DEFAULT TRUE,
    cname  TEXT,
    cpred  TEXT,
    -- trname cannot be empty but this default value is necessary
    trname  TEXT,
    trtout    INTERVAL, --TODO: fix this in the wed_trig trigger (copy this value from wed_trans)
    trsp    BOOL NOT NULL,
    scname  TEXT,
    scpred  TEXT
);
CREATE UNIQUE INDEX wed_trig_lower_trname_idx ON WED_trig (lower(trname), enabled) WHERE enabled is TRUE;
--CREATE UNIQUE INDEX wed_trig_cfinal_idx ON WED_trig (cfinal) WHERE cfinal is TRUE;


--Running transitions
CREATE TABLE JOB_POOL (
    wid     INTEGER NOT NULL ,
    tgid    INTEGER NOT NULL,
    trname   TEXT NOT NULL,
    trsp    BOOL NOT NULL,
    lckid   TEXT,
    created    TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    timeout    INTERVAL NOT NULL,
    payload JSON NOT NULL,
    PRIMARY KEY (wid,tgid),
    FOREIGN KEY (wid) REFERENCES WED_flow (wid) ON DELETE RESTRICT
);     

--Fast final WED-state detection(Running,Final,Exception)
CREATE TABLE ST_STATUS (
    wid     INTEGER PRIMARY KEY,
    status  TEXT NOT NULL DEFAULT 'R',
    FOREIGN KEY (wid) REFERENCES WED_flow (wid) ON DELETE CASCADE
);

--*WED-trace keeps the execution history for all instances
--trw: transition that wrote state
--trf: transitions fired by state
--trk: transitions killed by state
CREATE TABLE WED_trace (
    wid     INTEGER,
    trw    TEXT DEFAULT NULL,
    trf    TEXT[] DEFAULT NULL,
    trk    TEXT[] DEFAULT NULL,
    status    TEXT NOT NULL DEFAULT 'R',
    tstmp      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    state      JSON NOT NULL,
    FOREIGN KEY (wid) REFERENCES WED_flow (wid) ON DELETE RESTRICT
);
