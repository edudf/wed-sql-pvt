--CREATE LANGUAGE plpython3u;
--CREATE ROLE wed_admin WITH superuser noinherit;
--GRANT wed_admin TO wedflow;

--SET ROLE wed_admin;
--Insert (or modify) a new WED-atribute in the apropriate tables 
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION wed_attr_handler_aft() RETURNS TRIGGER AS 
$wah$
    #--plpy.info('Trigger "'+TD['name']+'" ('+TD['event']+','+TD['when']+') on "'+TD['table_name']+'"')
    if TD['event'] == 'INSERT':
        #--plpy.notice('Inserting new attribute: ' + TD['new']['name'])
        try:
            plpy.execute('ALTER TABLE wed_flow ADD COLUMN ' 
                         + plpy.quote_ident(TD['new']['aname']) 
                         + ' TEXT DEFAULT ' 
                         + (plpy.quote_literal(TD['new']['adv']) if TD['new']['adv'] else 'NULL'))
        except plpy.SPIError:
            plpy.error('Could not insert new column at wed_flow')
        else:
            plpy.info('Column "'+TD['new']['aname']+'" inserted into wed_flow')
            
    elif TD['event'] == 'UPDATE':
        if TD['new']['aname'] != TD['old']['aname']:
            
            plpy.error('It is not possible to rename an WED-attribute !')
            
        if TD['new']['adv'] != TD['old']['adv']:
            #--plpy.notice('Updating attribute '+TD['old']['name']+' default value :' 
            #--            + TD['old']['default_value'] + ' -> ' + TD['new']['default_value'])
            try:
                plpy.execute('ALTER TABLE wed_flow ALTER COLUMN ' 
                             + plpy.quote_ident(TD['new']['aname']) 
                             + ' SET DEFAULT ' 
                             + (plpy.quote_literal(TD['new']['adv']) if TD['new']['adv'] else 'NULL'))
            except plpy.SPIError:
                plpy.error('Could not modify columns at wed_flow')
            else:
                plpy.info('Column default value updated in wed_flow')
    
    #-- An attribute column can only be dropped if there aren't any pending transactions for all wed-states and it is not
    #--'referenced' by any predicate (cpred column in wed_trig table), otherwise an error should be raised.
    #--elif TD['event'] == 'DELETE':
    #--    try:
    #--        plpy.execute('ALTER TABLE wed_flow DROP COLUMN '+ plpy.quote_ident(TD['old']['aname'])) 
    #--    except plpy.SPIError:
    #--        plpy.error('Could not remove column %s at wed_flow' %(TD['old'['aname']))
    #--    else:
    #--        plpy.info('Column %s removed from wed_flow' %(TD['old'['aname']))
        
    else:
        plpy.error('UNDEFINED EVENT')
        return None
    return None    
$wah$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';

DROP TRIGGER IF EXISTS wed_attr_trg_aft ON wed_attr;
CREATE TRIGGER wed_attr_trg_aft
AFTER INSERT OR UPDATE ON wed_attr
    FOR EACH ROW EXECUTE PROCEDURE wed_attr_handler_aft();

--Insert a WED-flow modification into WED-trace (history)
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION kernel_function() RETURNS TRIGGER AS $kt$
    
    from os import urandom
    from datetime import datetime
    import hashlib
    import json
    from plpy import spiexceptions
    
    #--Generates new instance trigger token ----------------------------------------------------------------------------
    def new_uptkn(trigger_name):
        salt = urandom(5)
        hash = hashlib.md5(salt + trigger_name)
        return hash.hexdigest()

    #--Match predicates against the new state -------------------------------------------------------------------------
    def pred_match():
        
        trmatched = []
        try:
            res_wed_trig = plpy.execute('select * from wed_trig where enabled')
        except plpy.SPIError:
            plpy.error('wed_trig scan error')
        else:
            for tr in res_wed_trig:
                try:
                    res_wed_flow = plpy.execute('select * from wed_flow where wid='+str(TD['new']['wid'])+' and ('+tr['cpred']+')')
                except plpy.SPIError as e:
                    plpy.error('PREDICATE MATCH ERROR!\n PREDICATE:(%s) \n ERROR:(%s)' %(tr['cpred'],e))
                else:
                    if res_wed_flow:
                        trmatched.append((tr['trname'],tr['tgid'],tr['trsp'],tr['trtout']))
        return trmatched
    
    #--Match stop conditions predicates
    def sc_pred_match():
        
        trsc_matched = []
        try:
            res_wed_trig = plpy.execute('select * from wed_trig where enabled')
        except plpy.SPIError:
            plpy.error('wed_trig scan error')
        else:
            for tr in res_wed_trig:
                if tr['scpred']:
                    try:
                        res_wed_flow = plpy.execute('select * from wed_flow where wid='+str(TD['new']['wid'])+' and ('+tr['scpred']+')')
                    except plpy.SPIError as e:
                        plpy.error('STOP CONDITION PREDICATE MATCH ERROR!\n PREDICATE:(%s) \n ERROR:(%s)' %(tr['scpred'],e))
                   
                    if res_wed_flow:
                        trsc_matched.append((tr['trname'],tr['tgid']))
        return trsc_matched
    
    def json_wed_state():
        
        payload = TD['new'].copy()
        del payload['wid']
        
        return json.dumps(payload)
        
    def is_final(trmatched):
        return '_FINAL' in [x[0] for x in trmatched if x[0] == '_FINAL']
        
    #--Fire WED-triggers given a WED-condtions set  --------------------------------------------------------------------
    def squeeze_all_triggers(trmatched):
        
        trfired = []
        
        if is_final(trmatched):
            return trfired
        
        wid = TD['new']['wid']
        payload = json_wed_state()
        
        plan = plpy.prepare('insert into job_pool (wid,tgid,trname,trsp,timeout,payload) values ($1,$2,$3,$4,$5,$6)',['integer','integer','text','bool','interval','json'])
        
        for trname,tgid,trsp,timeout in trmatched:
            try:
                plpy.execute(plan,[wid,tgid,trname,trsp,timeout,payload])
            except spiexceptions.UniqueViolation:
                #--plpy.info('UNIQUE VIOLAtioN: JOB_POOL')
                pass
            except plpy.SPIError as e:
                plpy.error(e)
            else:
                trfired.append(trname)
                msg = {'wid':wid,'tgid':tgid,'trsp':trsp,'timeout':timeout,'payload':payload}
                try:
                    plpy.execute('NOTIFY '+trname+', \''+json.dumps(msg)+'\'')
                except plpy.SPIError as e:
                    plpy.notice('Notification error:',e)
                    
        return trfired
    
    def stop_transitions(trsc_matched):
        
        tr_killed = []
        
        for trname,tgid in trsc_matched:
            try:
                res = plpy.execute('select trstop('+str(TD['new']['wid'])+','+str(tgid)+')')
            except plpy.SPIError as e:
                    plpy.error('Transition kill error [%s]: %s' %(trname,e))
            if res[0]['trstop']:
                tr_killed.append(trname)
       
        return tr_killed
            
    #--Create a new entry on history (WED_trace table) -----------------------------------------------------------------
    def new_trace_entry(trw=None, trf=None, trk=None, status='R'):
             
        plan = plpy.prepare('INSERT INTO wed_trace (wid,trw,trf,trk,status,state) VALUES ($1,$2,$3,$4,$5,$6)',['integer','text','text[]','text[]','text','json'])
        try:
            plpy.execute(plan, [TD['new']['wid'],trw,trf,trk,status,json_wed_state()])
        except plpy.SPIError as e:
            plpy.info('Could not insert new entry into wed_trace')
            plpy.error(e)
    
    #-- Create a new entry on ST_STATUS for fast detecting final states ------------------------------------------------
    def new_st_status_entry():
        try:
            plpy.execute('INSERT INTO st_status (wid) VALUES (' + str(TD['new']['wid']) + ')')
        except plpy.SPIError as e:
            plpy.info('Could not insert new entry into st_status')
            plpy.error(e)    
    
    
    #-- Find job with uptkn on JOB_POOL (locked and inside timout window)-----------------------------------------------
    def find_job(pid):
        try:
            res = plpy.execute('select oid from pg_database where datname = current_database()')
        except plpy.SPIError as e:
            plpy.error(e)
        
        dbid = res[0]['oid']
        
        try:
            res = plpy.execute('select classid,objid from pg_locks where locktype=\'advisory\' and granted and database='+str(dbid)+' and pid='+str(pid))
        except plpy.SPIError as e:
            plpy.error(e)
        
        if not res:
            return None
        
        wid,tgid = res[0]['classid'], res[0]['objid']
        
        try:
            res = plpy.execute('select trname from job_pool where wid='+str(wid)+' and tgid='+str(tgid))
        except plpy.SPIError as e:
            plpy.error(e)
        
        if not res:
            return None
                
        return (wid,tgid,res[0]['trname'])
        
    def remove_job(wid,tgid):
        try:
            plpy.execute('delete from job_pool where wid='+str(wid)+' and tgid='+str(tgid))
        except plpy.SPIError as e:
            plpy.error(e)
                   
    #-- scan job_pool for pending transitions for WED-flow instance wid
    def check_for_pending_jobs():
        try:
            res = plpy.execute('select wid from job_pool where wid='+str(TD['new']['wid']))
        except plpy.SPIError:
            plpy.error('ERROR: job_pool scanning')
        
        return True if len(res) > 0 else False
        
    
    #-- Check if a given wed-flow instance is already on a final state -------------------------------------------------
    def get_st_status():
        try:
            res = plpy.execute('select status from st_status where wid='+str(TD['new']['wid']))
        except plpy.SPIError:
            plpy.error('Reading st_status')
        else:
            if not len(res):
                plpy.error('wid not found !')
            else:
                return res[0]['status']
    
    #-- Set an WED-state status (final or not final)
    def set_st_status(status='R'):
        try:
            res = plpy.execute('update st_status set status=\''+status+'\' where wid='+str(TD['new']['wid']))
        except plpy.SPIError:
            plpy.error('Status set error on st_status table')

    def get_worker_pid():
        try:
            res = plpy.execute('select pg_backend_pid() as pid')
        except plpy.SPIError:
            plpy.error('Error: identifying worker !')
        
        return res[0]['pid']
        
    def terminate_worker(pid):
        try:
            res = plpy.execute('select pg_terminate_backend('+str(pid)+')')
        except plpy.SPIError:
            plpy.error('Error: terminating worker (pid:'+str(pid)+')')
        
        plpy.info(res)
        
        
    #--(START) TRIGGER CODE --------------------------------------------------------------------------------------------

               
    
    #-- New wed-flow instance (AFTER INSERT)----------------------------------------------------------------------------
    if TD['event'] in ['INSERT']:
        
        trmatched = pred_match()
        trsc_matched = sc_pred_match()
        
        if (not trmatched):
            plpy.error('This initial WED-state did not fire any WED-trigger, aborting ...')
        
        if is_final(trmatched):
            status = 'F'
        else:
            status = 'R'
        
        trfired = squeeze_all_triggers(trmatched)
        tr_killed = stop_transitions(trsc_matched)
        new_st_status_entry()
        
        #--all inititaly fired transitions were killed
        if set(trfired) == set(tr_killed):
            plpy.error('All fired WED-Transitions were immediately killed. Aborting ...')
            
        new_trace_entry('_INIT_', trfired, tr_killed, status)
        set_st_status(status)
        
        for t in tr_killed:
            new_trace_entry('_EOUT_'+t, [], [], status)
        
        return "OK"
        
            

    #-- Updating an WED-state ------------------------------------------------------------------------------------------
    elif TD['event'] in ['UPDATE']:
        
        status = get_st_status()
        
        if status == 'F':
            plpy.error('Cannot modify a final WED-state !')
        
        #-- check if the transaction is the same that set the advisory lock (transaction still open)
        pid = get_worker_pid()
        job =  find_job(pid)
        
        if not job:
            plpy.error('Job not found !')
        
        if job[0] != TD['new']['wid']:
            plpy.error('Invalid update !')
        
        #--validations and match
        trmatched = pred_match()
        remove_job(job[0],job[1])
        trfired = squeeze_all_triggers(trmatched)
        
        #--kill transitions on sc
        trsc_matched = sc_pred_match()
        tr_killed = stop_transitions(trsc_matched)
        
        
        final = is_final(trmatched)
        pj = check_for_pending_jobs()
        
        
        if final and pj:
            plpy.error('Impossible to set a final WED-state if there are others pending WED-transactions for this instance')
        elif not (trfired or pj or final):
            plpy.info('Inconsistent WED-state detected')
            status = 'E'
            #--lanch an exception job
            squeeze_all_triggers([('_EXCPT',job[1],'1Y')])
        else:
            status = 'F' if final else 'R'
        
        new_trace_entry(job[2], trfired, tr_killed, status)
        set_st_status(status)
        
        for t in tr_killed:
            new_trace_entry('_EOUT_'+t, [], [], status)
        
        return "OK"
        
       
    #--(END) TRIGGER CODE ----------------------------------------------------------------------------------------------    
$kt$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';

DROP TRIGGER IF EXISTS kernel_trigger ON wed_flow;
CREATE TRIGGER kernel_trigger
AFTER INSERT OR UPDATE ON wed_flow
    FOR EACH ROW EXECUTE PROCEDURE kernel_function();

CREATE OR REPLACE FUNCTION sp_transition_trigger() RETURNS TRIGGER as $spt$
    
    plpy.log('INSERT ON JOB_POOL: %s' % TD['new'])
    try:
        lock_res = plpy.execute('select pg_try_advisory_xact_lock(%d, %d) as lock' % (TD['new']['wid'], TD['new']['tgid']))
    except plpy.SPIError as e:
        plpy.log('SP_TRANSITION_TRIGGER locking error: %s' % srt(e))
    
    if not lock_res[0]['lock']:
        plpy.warning('JOB wid=%d, tgid=%d already locked ! Stored WED-transition %s won\'t executed.' % (TD['new']['wid'], TD['new']['tgid']))
    else:
        #--FIXME: plpy.prepare complains if plan string contains $ signs that are not parameters
        #--if 'sp_trans_plan' in SD:
        #--    sp_trans_plan = SD['sp_trans_plan']
        #--else:
        #--    sp_trans_plan = plpy.prepare('select pg_background_launch($bgq$select sp_worker($wst$ $1 $wst$, $2, $3, $4);$bgq$)', ['text', 'text', 'integer', 'integer'])
        #--    SD['sp_trans_plan'] = sp_trans_plan
        try:
            #--plpy.execute(sp_trans_plan, [TD['new']['payload'], TD['new']['trname'], TD['new']['wid'], TD['new']['tgid']])
            plpy.execute('select pg_background_launch($bgq$select sp_worker($wst$ %s $wst$, \'%s\', %d, %d);$bgq$)' % (TD['new']['payload'], TD['new']['trname'], TD['new']['wid'], TD['new']['tgid']))
        except plpy.SPIError as e:
            plpy.log('SP_TRANSITION_TRIGGER launch error: %s' % srt(e))
    
    return "OK"
    
$spt$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';

DROP TRIGGER IF EXISTS sp_transitions ON job_pool;
CREATE TRIGGER sp_transitions
AFTER INSERT ON job_pool
    FOR EACH ROW WHEN (NEW.trsp = TRUE) EXECUTE PROCEDURE sp_transition_trigger();

CREATE OR REPLACE FUNCTION wed_trig_insert() RETURNS TRIGGER AS $wti$
     
    if TD['event'] == "INSERT":
        try:
            #--check if the wed-condition exists
            cond = plpy.execute('select * from wed_cond where cname = \'' + TD['new']['cname'] +'\'')
        except plpy.SPIError as e:
            plpy.error(e)
      
        if cond:
            TD['new']['cpred'] = cond[0]['cpred']
        else:
            plpy.error('WED-Condition %s not found!' %(TD['new']['cname']))
        

        try:
            #--do the same for the wed-transition
            trans = plpy.execute('select * from wed_trans where trname = \'' + TD['new']['trname'] +'\'')
        except plpy.SPIError as e:
            plpy.error(e)
      
        if trans:
            TD['new']['trtout'] = trans[0]['trtout']
            TD['new']['trsp'] = trans[0]['trsp']
            
            #--fetch the stop condition 
            if trans[0]['trsc']:
                try:
                    #--check if the wed-condition exists
                    sc = plpy.execute('select * from wed_cond where cname = \'' + trans[0]['trsc'] +'\'')
                except plpy.SPIError as e:
                    plpy.error(e)
                
                TD['new']['scname'] = sc[0]['cname']
                TD['new']['scpred'] = sc[0]['cpred']
        
            return "MODIFY"
        else:
            plpy.error('WED-Transition %s not found!' %(TD['new']['trname']))
    
    return "SKIP"
        
    
$wti$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';
   
        
DROP TRIGGER IF EXISTS wed_trig_insert_trg ON wed_trig;
CREATE TRIGGER wed_trig_insert_trg
BEFORE INSERT ON wed_trig
    FOR EACH ROW EXECUTE PROCEDURE wed_trig_insert();
  

------------------------------------------------------------------------------------------------------------------------
-- Validate predicate (cpred) in WED_cond table

CREATE OR REPLACE FUNCTION wed_cond_validation_bfe() RETURNS TRIGGER AS $wcv$
    
    if TD['event'] in ['INSERT']:       
        import re
            
        fbdtkn = re.compile(r'CREATE|DROP|ALTER|GRANT|REVOKE|SELECT|INSERT|UPDATE|DELETE|;',re.I)        
        found = fbdtkn.search(TD['new']['cpred'])
        if found:
            plpy.error('Forbidden character or SQL keyword found in cpred expression: '+ found.group(0))
            #--return "SKIP"
        
        if TD['new']['cname']:
            trname = re.compile(r'^_')
            sysname = trname.search(TD['new']['cname'])
            if sysname:
                plpy.error('trname must not start with an underscore character !')
                #--return "SKIP"
        #--check for invalid wed-attributes references in predicate
        if not TD['new']['cpred']:
            plpy.error('cpred cannot be empty !')
        try:
            res = plpy.execute('select True from wed_flow where '+TD['new']['cpred']+ ' limit 1')
        except plpy.SPIError as e:
            plpy.error('Invalid predicate: %s' %(e)) 
    
    if TD['event'] in ['UPDATE']:
        return "SKIP"
    
$wcv$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';

DROP TRIGGER IF EXISTS wed_cond_bfe ON wed_cond;
CREATE TRIGGER wed_cond_bfe
BEFORE INSERT OR UPDATE ON wed_cond
    FOR EACH ROW EXECUTE PROCEDURE wed_cond_validation_bfe();

------------------------------------------------------------------------------------------------------------------------
-- Validate timeout (trtout) in WED_trans table

CREATE OR REPLACE FUNCTION wed_trans_validation_bfe() RETURNS TRIGGER AS $wtsv$    
    import re

    if TD['event'] in ['INSERT']:       
        
        if TD['new']['trname']:
            trname = re.compile(r'^_')
            sysname = trname.search(TD['new']['trname'])
            if sysname:
                plpy.error('trname must not start with an underscore character!')
                #--return "SKIP"
        else:
            plpy.error('WED-transition name cannot by empty!')
        
        if TD['new']['trtout'] == '00:00:00':
            plpy.error('timeout cannot be empty!')
        if TD['new']['trsc']:
            try:
                cond_chk = plpy.execute('select * from wed_cond where cname = \'' + TD['new']['trsc'] +'\'')
            except plpy.SPIError as e:
                plpy.error(e)
            
            if not cond_chk:
                plpy.error('stop condition ' + TD['new']['trsc'] + ' not found!')
    
    if TD['event'] in ['UPDATE']:
        #--TODO: update wed_trig table
        return "SKIP"
        
$wtsv$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';

DROP TRIGGER IF EXISTS wed_trans_bfe ON wed_trans;
CREATE TRIGGER wed_trans_bfe
BEFORE INSERT OR UPDATE ON wed_trans
    FOR EACH ROW EXECUTE PROCEDURE wed_trans_validation_bfe();

CREATE OR REPLACE FUNCTION wed_cond_remove() RETURNS TRIGGER AS $wcr$
    
    #--TODO: check if condition is a stop condition before remove
    if TD['event'] in ['DELETE']:
        res = plpy.execute('select tgname from wed_trig where cname=\''+TD['old']['cname']+'\' limit 1')
        if res:
            plpy.error('WED-condition is active in WED-trigger "%s". Cannot remove !' %(res[0]['tgname']))
            return "SKIP"
        else:
            return "OK"
    
    return "SKIP"
    
$wcr$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';
DROP TRIGGER IF EXISTS wed_cond_remove_bfe ON wed_cond;
CREATE TRIGGER wed_cond_remove_bfe
BEFORE DELETE ON wed_cond
    FOR EACH ROW EXECUTE PROCEDURE wed_cond_remove();
    
CREATE OR REPLACE FUNCTION wed_trans_remove() RETURNS TRIGGER AS $wtr$
    
    if TD['event'] in ['DELETE']:
        res = plpy.execute('select tgname from wed_trig where trname=\''+TD['old']['trname']+'\' limit 1')
        if res:
            plpy.error('WED-transition is active in WED-trigger "%s". Cannot remove !' %(res[0]['tgname']))
            return "SKIP"
        else:
            return "OK"
    
    return "SKIP"
    
$wtr$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';
DROP TRIGGER IF EXISTS wed_trans_remove_bfe ON wed_trans;
CREATE TRIGGER wed_trans_remove_bfe
BEFORE DELETE ON wed_trans
    FOR EACH ROW EXECUTE PROCEDURE wed_trans_remove();

--RESET ROLE;


