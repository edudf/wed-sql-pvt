import sys
from antlr4 import *

class WedsqlSemantic():

    def __init__(self):
        self.condition_set = []
        self.transition_set = []
        self.running_function = None
    
    def prepare_semantic_validation(self, running_function):
        self.running_function = running_function
        self.condition_set = self.get_conditions()
        self.transition_set = self.get_transitions()
    
    def get_conditions(self):
        sql_command = 'select cname from wed_cond;'
        results = self.running_function(sql_command)
        formatted_results = [j for i in results for j in i]
        return formatted_results

    def get_transitions(self):
        sql_command = 'select trname from wed_trans;'
        results = self.running_function(sql_command)
        formatted_results = [j for i in results for j in i]
        return formatted_results
    
    def add_condition(self, cond_id):
        self.condition_set.append(cond_id)
    
    def add_transition(self, tran_id):
        self.transition_set.append(tran_id)
        
    def validate_condition_creation(self, ctx):
        condition_expr = ctx.CONDITION_EXPR().getText().strip()
        condition_parts = condition_expr.split(None, 3)
        condition_name = condition_parts[1].strip()
        self.add_condition(condition_name)
        
    def validate_trigger_creation(self, ctx):
        trigger_id = ctx.valid_name().getText()
        trigger_expr = ctx.trigger_expr()
        cond_id = trigger_expr.valid_name(0).getText()
        trans_id = trigger_expr.valid_name(1).getText()
        error = ""
        if cond_id not in self.condition_set:
            error += "Error: Error while attempting to create trigger '{}', condition '{}' does not exist.\n".format(trigger_id, cond_id)
        if trans_id not in self.transition_set:
            error += "Error: Error while attempting to create trigger '{}', transition '{}' does not exist.\n".format(trigger_id, trans_id)
        return error

    def validate_transition_creation(self, ctx):
        transition_name = ctx.valid_name().getText()
        stop_expr = ctx.opt_stop_expr()
        error = ""
        if stop_expr.getText():
            stop_cond_expr = stop_expr.stop_condition_expr()
            if stop_cond_expr != None:
                stop_condition = stop_cond_expr.valid_name().getText().strip()
                if stop_condition not in self.condition_set:
                    error += "Error: Error while attempting to create transition '{}', stop condition '{}' does not exist.\n".format(transition_name, stop_condition)
        if error == "":
            self.add_transition(transition_name)
        return error

