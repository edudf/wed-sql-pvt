# encoding: utf-8
import sys
import os
import psycopg2
from antlr4 import *
sys.path.insert(0, sys.path[0]+'/../../compilers/wedsql_compiler')
from Wedsql import Wedsql 
from WedsqlLexer import WedsqlLexer 
from WedsqlListener import WedsqlListener 
from WedsqlTranslator import WedsqlTranslator

class Wedsql2Sql(WedsqlListener):

    def __init__(self, input_file_name):
        output_base = os.path.basename(input_file_name)
        self.output_file_name = os.path.splitext(output_base)[0] + ".sql"
        self.output = open(self.output_file_name, 'w')
        self.translator = WedsqlTranslator()
        self.pg_connection = None

    def enterShell_connect_flow_stmt(self, ctx):
        database = ctx.valid_name().getText()
        self.output.write("\cw "+database+";\n")
        try:
            conn = psycopg2.connect(database=database, user=database, password="")
        except Exception as e:
            print("Connection Error: could not connect to wed-flow \"%s\"" %(database))
        else:
            if self.pg_connection != None:
                self.pg_connection.close()
                self.pg_connection = None
            self.pg_connection = conn

    def enterCreate_flow_stmt(self, ctx):
        print("Warning: The WED-flow creation command contains steps that can't be written as SQL commands, and therefore is ignored by the wsql2sql tool. Use the wsql program to run WED-flow creation.\n")

    def enterRemove_flow_stmt(self, ctx):
        print("Warning: The WED-flow removal command contains steps that can't be written as SQL commands, and therefore is ignored by the wsql2sql tool. Use the wsql program to run WED-flow removal.\n")

    def enterCreate_attr_stmt(self, ctx):
        (stmt, params) = self.translator.create_attribute(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterCreate_cond_stmt(self, ctx):
        (stmt, params) = self.translator.create_condition(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterCreate_trans_stmt(self, ctx):
        (stmt, params) = self.translator.create_transition(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterCreate_trig_stmt(self, ctx):
        (stmt, params) = self.translator.create_trigger(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterCreate_inst_stmt(self, ctx):
        (stmt, params) = self.translator.create_instance(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterSet_fstate_stmt(self, ctx):
        (stmt, params) = self.translator.set_final_state(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterEnable_trig_stmt(self, ctx):
        (stmt, params) = self.translator.enable_trigger(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterDisable_trig_stmt(self, ctx):
        (stmt, params) = self.translator.disable_trigger(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterList_attr_stmt(self, ctx):
        (stmt, params) = self.translator.list_attributes(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterList_cond_stmt(self, ctx):
        (stmt, params) = self.translator.list_conditions(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterList_trans_stmt(self, ctx):
        (stmt, params) = self.translator.list_transitions(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterList_trig_stmt(self, ctx):
        (stmt, params) = self.translator.list_triggers(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterList_ftrans_stmt(self, ctx):
        (stmt, params) = self.translator.list_fired_transitions(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterList_insts_stmt(self, ctx):
        (stmt, params) = self.translator.list_instances(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterShow_fstate_stmt(self, ctx):
        (stmt, params) = self.translator.show_final_state(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def enterGet_trace_stmt(self, ctx):
        (stmt, params) = self.translator.get_trace(ctx)
        stmt += "\n"
        self.write_output(stmt, params)

    def write_output(self, stmt, params):
        cur = self.pg_connection.cursor()
        stmt = cur.mogrify(stmt, params)
        cur.close()
        self.output.write(stmt.decode("utf-8"))

    def exitWsql(self, ctx):
        self.pg_connection.close()
        self.pg_connection = None
        self.output.close()

def main(argv):
    input = FileStream(argv[1], encoding="utf-8")
    lexer = WedsqlLexer(input)
    stream = CommonTokenStream(lexer)
    parser = Wedsql(stream)
    tree = parser.wsql()
    printer = Wedsql2Sql(argv[1])
    walker = ParseTreeWalker()
    walker.walk(printer, tree)

if __name__ == '__main__':
    main(sys.argv)
