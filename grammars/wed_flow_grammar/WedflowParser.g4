parser grammar WedflowParser;
options { tokenVocab=WedflowLexer; }

wflow: stmt_list;

stmt_list: empty_space stmt empty_space stmt_list 
         | empty_space stmt empty_space;

stmt: create_flow_stmt empty_space SEMICOLON
    | create_attr_stmt empty_space SEMICOLON 
    | create_cond_stmt empty_space
    | create_trans_stmt empty_space SEMICOLON
    | create_trig_stmt empty_space;

create_flow_stmt: annotation_wed_flow SPACE* LINEBREAK empty_space valid_name empty_space EQUALS empty_space wed_flow_set_expr;

create_attr_stmt: annotation_wed_attributes SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr;

create_cond_stmt: ANNOTATION_WED_CONDITIONS WC_SPACE* WC_LINEBREAK wc_empty_space WC_SET_NAME wc_empty_space WC_EQUALS wc_empty_space wc_set_expr wc_empty_space WC_SEMICOLON wc_empty_space (condition_def wc_empty_space)*;

create_trans_stmt: annotation_wed_transitions SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr empty_space;

create_trig_stmt: annotation_wed_triggers SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr empty_space SEMICOLON empty_space (trigger_def empty_space)*;

annotation_wed_flow : (WC_ANNOTATION_WED_FLOW | ANNOTATION_WED_FLOW);

annotation_wed_attributes : (WC_ANNOTATION_WED_ATTRIBUTES | ANNOTATION_WED_ATTRIBUTES);

annotation_wed_transitions : (WC_ANNOTATION_WED_TRANSITIONS | ANNOTATION_WED_TRANSITIONS);

annotation_wed_triggers : (WC_ANNOTATION_WED_TRIGGERS | ANNOTATION_WED_TRIGGERS);

set_expr : OPEN_CURLY_BRACKETS empty_space ((valid_name empty_space COMMA empty_space)* valid_name empty_space)? CLOSE_CURLY_BRACKETS;

wed_flow_set_expr : OPEN_CURLY_BRACKETS empty_space SET_NAME empty_space COMMA empty_space valid_name empty_space COMMA empty_space  valid_name empty_space CLOSE_CURLY_BRACKETS;

trigger_def : valid_name empty_space COLON empty_space OPEN_PARENTHESES empty_space valid_name empty_space COMMA empty_space valid_name empty_space CLOSE_PARENTHESES empty_space SEMICOLON;

valid_name : (SET_NAME | VALID_NAME);

empty_space : (SPACE | LINEBREAK)*;

wc_set_expr : WC_OPEN_CURLY_BRACKETS wc_empty_space ((wc_valid_name wc_empty_space WC_COMMA wc_empty_space)* wc_valid_name wc_empty_space)? WC_CLOSE_CURLY_BRACKETS;

condition_def : wc_valid_name wc_empty_space PREDICATE;

wc_valid_name : (WC_SET_NAME | WC_VALID_NAME);

wc_empty_space : (WC_SPACE | WC_LINEBREAK)*;
