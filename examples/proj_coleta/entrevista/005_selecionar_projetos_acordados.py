from BaseWorker import BaseClass
import sys

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'selecionar_projetos_acordados'
    dbs = 'user=entrevista dbname=entrevista application_name=ww-tr_ent'
    wakeup_interval = 50
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
        self.proj_table = {'t_pjx': '{\"projeto\":\"pjx\", \"material\":\"cerebro\"}',
                           't_pjy': '{\"projeto\":\"pjy\", \"material\":\"rim\"}',
                           't_pjk': '{\"projeto\":\"pjk\", \"material\":\"coracao\"}',
                           't_pjz': '{\"projeto\":\"pjz\", \"material\":\"figado\"}'}
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        projetos_acordados = []
        termos = payload['termos_consentidos'].lstrip('{').rstrip('}').split(',')
        
        for t in termos:
            projetos_acordados.append(self.proj_table[t])
        
        return "projetos_acordados='[%s]', elegivel='true'" % (','.join(projetos_acordados))
        #return None
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

