# encoding: utf-8
import sys
import os
from antlr4 import *
from antlr4.tree.Trees import Trees
sys.path.insert(0, sys.path[0]+'/../../compilers/wedflow_compiler')
from Wedflow import Wedflow 
from WedflowLexer import WedflowLexer 
from WedflowListener import WedflowListener 
from WedflowTranslator import WedflowTranslator 
from WedflowSemantic import WedflowSemantic 

class Wedflow2Wedsql(WedflowListener):

    cond_def = {}
    transition_set = []
    final_condition = ""
    textCreateFlow = ""
    textCreateAttributes = ""
    textCreateConditions = ""
    textCreateTransitions = ""
    textCreateTriggers = ""
    textSetFinalState = ""
        
    def __init__(self, input_file_name):
        output_base = os.path.basename(input_file_name)
        self.output_file_name = os.path.splitext(output_base)[0] + ".wsql"
        self.output = open(self.output_file_name, 'w')
        self.translator = WedflowTranslator()
        self.semantic = WedflowSemantic()
        self.errors = ""

    def enterCreate_flow_stmt(self, ctx):
        final_condition = ctx.wed_flow_set_expr().valid_name()[1].getText()
        self.final_condition = final_condition
        self.semantic.read_create_flow(ctx)
        self.textCreateFlow = self.translator.create_flow(ctx)
        self.textCreateConditions = self.translator.create_conditions(final_condition)

    def enterCreate_attr_stmt(self, ctx):
        self.textCreateAttributes += self.translator.create_attributes(ctx)

    def enterCreate_cond_stmt(self, ctx):
        self.errors += self.semantic.check_condition_set(ctx)
        self.translator.prepare_conditions(ctx)

    def enterCreate_trans_stmt(self, ctx):
        transition_name_list = ctx.set_expr().valid_name()
        self.transition_set = [t.getText() for t in transition_name_list]
        self.textCreateTransitions = self.translator.create_transitions(ctx)

    def enterCreate_trig_stmt(self, ctx):
        self.errors += self.semantic.check_trigger_set(ctx)
        self.textCreateTriggers = self.translator.create_triggers(ctx)

    def exitWflow(self, ctx):
        self.errors += self.semantic.check_awic(self.final_condition)
        self.errors += self.semantic.check_flow()
        self.errors += self.semantic.check_triggers(self.transition_set)
        if self.errors == "":
            self.textSetFinalState = self.translator.set_final_state(self.final_condition)
            self.output.write(self.textCreateFlow)
            self.output.write(self.textCreateAttributes)
            self.output.write(self.textCreateConditions)
            self.output.write(self.textCreateTransitions)
            self.output.write(self.textCreateTriggers)
            self.output.write(self.textSetFinalState)
            self.output.write("commit;\n")
        else:
            print(self.errors)
        self.output.close()

def main(argv):
    input = FileStream(argv[1], encoding="utf-8")
    lexer = WedflowLexer(input)
    stream = CommonTokenStream(lexer)
    parser = Wedflow(stream)
    tree = parser.wflow()
    printer = Wedflow2Wedsql(argv[1])
    walker = ParseTreeWalker()
    walker.walk(printer, tree)

if __name__ == '__main__':
    main(sys.argv)
