#!/bin/bash

TEMPLATE='saga_timeout_template'
DB=$1
WORKER=${DB}_saga_timeout
CONFIG=$3
USER=$2
ADMIN_USER=admin_${USER}

if [[ $# < 3 ]]
then
	echo "$0 <database name> <user name> <postgresql.conf file>"
	exit 1
elif [[ $UID != 0 ]]
then
	echo "Need to be root!"
	exit 1
elif [[ ! -f $3 ]]
then
	echo "File $3 not found"
	exit 1
fi
echo -e "\033[33;1mWARNING !!\033[0m This operation is about to OBLITERATE the WED-flow $DB"
echo -ne "Are you absolute sure you want to continue ? [YES/No]: "
read ANS

if [[ $ANS != "YES" ]]
then
    echo -e "\nNo harm done ... see ya."
    exit 1
fi

echo -ne "Oh boy ... . All right, type pi with six decimal digits : "
read PI

if [[ $PI != "3.141592" ]]
then
    echo -e "\nThat was close ! Maybe you're not so sure what ya doing. Try again."
    exit 1
fi

echo -e "Removing bg_worker ...\n"
python3 pg_worker_unregister.py $WORKER $CONFIG 
if [[ $? == 0 ]]
then
	rm -f '/usr/share/postgresql/extension'/${WORKER}.control
    rm -f '/usr/share/postgresql/extension'/${WORKER}--1.0.sql
    rm -f '/usr/lib/postgresql'/${WORKER}.so

	echo -e "Restarting postgresql server ..."
	systemctl restart postgresql
    if [[ $? != 0 ]]
    then
        /etc/init.d/postgresql restart
    fi
	echo -e "Removing database $DB ..."
	echo -e "DROP OWNED BY $USER ;
	         DROP DATABASE $DB ;
	         DROP ROLE $USER ;
	         DROP ROLE $ADMIN_USER ;" > tmp_drop
	psql -U postgres -f tmp_drop
	rm -f tmp_drop
fi

echo "DONE !"
exit 0
