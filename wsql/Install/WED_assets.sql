--SET ROLE wed_admin;

-- # This function enforces that wed-transitions must complete in the designated timeout
CREATE OR REPLACE FUNCTION trcheck () RETURNS bool AS 
$$
    import time
    
    rv = False
    #-- list of expired jobs
    e = []
    #-- list of valid (not expired) jobs
    v = []
    #-- list of instance that must be terminated due to all pending transitions been expired
    f = set()
    
    #-- look for active expired transactions
    try:
        kill_list = plpy.execute('select l.pid from pg_stat_activity a join pg_locks l '+ 
                                 'on a.pid = l.pid and l.locktype=\'advisory\' and l.granted and a.datname= current_database() '+
                                 'join job_pool j on l.classid = j.wid and l.objid=j.tgid where (j.created + j.timeout) < now()')
    except plpy.SPIError as e:
        plpy.error('TRCHECK ERROR: '+str(e))
    
    
    for job in kill_list:
        try:
            plpy.execute('select pg_terminate_backend('+str(job['pid'])+') as terminated')
        except plpy.SPIError as e:
            plpy.error(e)
        rv = True
    
    #-- look for expired jobs
    try:
        jobs = plpy.execute('select *, case when timeout - (now() - created) < \'0\'::interval then \'e\' else \'v\' end as status from job_pool')
    except plpy.SPIError as e:
        plpy.error('TRCHECK ERROR: '+str(e))
    
    for j in jobs:
        if j['status'] == 'e':
            e.append(j)
        else:
            v.append(j)
    plpy.info(e)
    for j in e:
            if not any(j['wid'] == tmp['wid'] for tmp in v):
                jj = max((i for i in e if i['wid'] == j['wid']), key=lambda x:x['created'])
                f.add((jj['wid'],jj['tgid']))
    
    if f:
        for i in f:
            e.append(e.pop(e.index(next(d for d in e if d['wid'] == i[0] and d['tgid'] == i[1]))))
    
    if e:
        if 'plan_rem_job' in SD:
            plan_rem_job = SD['plan_rem_job']
        else:
            plan_rem_job = plpy.prepare('delete from job_pool where wid=$1 and tgid=$2', ['integer', 'integer'])
            SD['plan_rem_job'] = plan_rem_job
            
        if 'plan_add_trace' in SD:
            plan_add_trace = SD['plan_add_trace']
        else:
            plan_add_trace = plpy.prepare('insert into wed_trace (wid,trw,trf,status,state) values ($1,$2,$3,$4,$5)',['integer','text','text[]','text','json'])
            SD['plan_add_trace'] = plan_add_trace
        
        try:
            with plpy.subtransaction():
                for i in e:
                    plpy.execute(plan_rem_job, [i['wid'], i['tgid']])
                    plpy.execute(plan_add_trace, [i['wid'], ('_TOUT_'+i['trname']), [], ('E' if (i['wid'], i['tgid']) in f else 'R'), i['payload']])
                if f:
                    if 'plan_end_inst' in SD:
                        plan_end_inst = SD['plan_end_inst']
                        plan_end_st = SD['plan_end_st']
                    else:
                        plan_end_inst = plpy.prepare('insert into job_pool (wid,tgid,trname,timeout,payload) values ($1,$2,$3,$4,$5)',['integer','integer','text','interval','json'])
                        plan_end_st = plpy.prepare('update st_status set status=\'E\' where wid=$1', ['integer'])
                        SD['plan_end_inst'] = plan_end_inst
                        SD['plan_end_st'] = plan_end_st
                    
                    for i in f:
                        full_job = next((item for item in e if item['wid'] == i[0] and item['tgid'] == i[1]), None)
                        plpy.execute(plan_end_inst, [i[0], i[1], '_EXCPT', '1Y', full_job['payload']])
                        plpy.execute(plan_end_st, [i[0]])
        except plpy.SPIError as e:
            plpy.error('TRCHECK ERROR: '+str(e))
        
        rv = True   

    return rv
    
$$ LANGUAGE plpython3u SET search_path = 'public';
------------------------------------------------------------------------------------------------------------------------

-- # Manualy terminate an instance (regardless of status) 
CREATE OR REPLACE FUNCTION terminate_instance(wid integer) RETURNS bool AS 
$$
    #-- check if there are running transitions
    try:
        active_transactions = plpy.execute('select l.pid from pg_stat_activity a join pg_locks l '+ 
                                 'on a.pid = l.pid and l.locktype=\'advisory\' and l.granted and a.datname= current_database() '+
                                 'join job_pool j on l.classid = j.wid and l.objid=j.tgid where j.wid='+str(wid))
    except plpy.SPIError as e:
        plpy.error('terminate_instance: '+str(e))
        
    plpy.info('Active transaction found: ' + str(active_transactions))
    
    try:
        with plpy.subtransaction():
            
            #-- remove jobs from job_pool
            deleted_jobs = plpy.execute('delete from job_pool where wid=%d returning trname' % (wid))
        
            plpy.info('Jobs removed: %d' % (deleted_jobs.nrows()))
            
            if not deleted_jobs.nrows():
                return False
            
            jobs = [t['trname'] for t in deleted_jobs]
            plpy.notice(jobs)
            
            #-- kill active transaction if any
            if 'kill_plan' in SD:
                    kill_plan = SD['kill_plan']
            else:
                kill_plan = plpy.prepare('select pg_terminate_backend($1) as terminated', ['integer'])
                SD['kill_plan'] = kill_plan
            
            for t in active_transactions:
                plpy.execute(kill_plan, [t['pid']])
    
            #-- insert wed_trace entry
            if 'trace_plan' in SD:
                    trace_plan = SD['trace_plan']
            else:
                trace_plan = plpy.prepare('INSERT INTO wed_trace (wid,trw,trf,trk,status,state) VALUES ($1,$2,$3,$4,$5,$6)',['integer','text','text[]','text[]','text','json'])
                plpy.execute(trace_plan, [wid, '_MANUAL_TERMINATION', [], jobs, 'T', '{}'])
            
            if 'status_plan' in SD:
                status_plan = SD['status_plan']
            else:
                status_plan = plpy.prepare('UPDATE st_status SET status = $1 where wid = $2', ['text', 'integer'])
                plpy.execute(status_plan, ['T', wid])
        
    except plpy.SPIError as e:
        plpy.error('terminate_instance: '+str(e))
    
    return True
$$ LANGUAGE plpython3u SET search_path = 'public';

CREATE OR REPLACE FUNCTION reinstate_instance (wid integer) RETURNS bool AS
$$
    try:
        status = plpy.execute('select status from st_status where wid = %d' % (wid))
    except plpy.SPIError as e:
        plpy.error('reinstate_instance: ' + str(e))
        
    if (not status) or status[0]['status'] != 'T':
        plpy.info('instance %d not found or not manually terminated.' % (wid))
        return False
    
    try:
        with plpy.subtransaction():
            if 'trace_plan' in SD:
                    trace_plan = SD['trace_plan']
            else:
                trace_plan = plpy.prepare('INSERT INTO wed_trace (wid,trw,trf,trk,status,state) VALUES ($1,$2,$3,$4,$5,$6)',['integer','text','text[]','text[]','text','json'])
                plpy.execute(trace_plan, [wid, '_REINSTATEMENT', [], [], 'E', '{}'])
            
            if 'status_plan' in SD:
                status_plan = SD['status_plan']
            else:
                status_plan = plpy.prepare('UPDATE st_status SET status = $1 where wid = $2', ['text', 'integer'])
                plpy.execute(status_plan, ['E', wid])
            
            if 'job_plan' in SD:
                job_plan = SD['job_plan']
            else:
                trsp = plpy.execute('select trsp from wed_trig where tgid = 0')
                job_plan = plpy.prepare('INSERT INTO job_pool (wid,tgid,trname,trsp,timeout,payload) VALUES ($1,$2,$3,$4,$5,$6)',['integer','integer','text','bool','interval','json'])
                plpy.execute(job_plan, [wid, 0, '_EXCPT', trsp, '1Y', '{}'])
                
    except plpy.SPIError as e:
        plpy.error('reinstate_instance: '+str(e))
        
    return True;

$$ LANGUAGE plpython3u SET search_path = 'public';

-- # This function terminates a transition on stop condition
CREATE OR REPLACE FUNCTION trstop (wid integer, tgid integer) RETURNS bool AS 
$$
    #--plpy.info('%d, %d' %(wid,tgid))
    
    #--find job
    try:
        job = plpy.execute('select * from job_pool where trname <> \'_EXCPT\' and wid='+str(wid)+' and tgid='+str(tgid))
    except plpy.SPIError as e:
        plpy.error('TRSTOP ERROR: '+str(e))
    
    if not job:
        return False
        
    #-- check if the transition is running
    try:
        active_transaction = plpy.execute('select l.pid from pg_stat_activity a join pg_locks l '+ 
                                 'on a.pid = l.pid and l.locktype=\'advisory\' and l.granted and a.datname= current_database() '+
                                 'join job_pool j on l.classid = j.wid and l.objid=j.tgid where j.wid='+str(wid)+' and j.tgid='+str(tgid))
    except plpy.SPIError as e:
        plpy.error('TRSTOP ERROR: '+str(e))
    
    #-- then kill it
    if active_transaction:
        plpy.info('live tr found with pid: %s' % (active_transaction[0]['pid']))
        try:
            terminated = plpy.execute('select pg_terminate_backend('+str(active_transaction[0]['pid'])+') as terminated')
        except plpy.SPIError as e:
            plpy.error(e)
    
    #-- remove it from job_pool and add trace entry
    if 'plan_del_job' in SD:
            plan_del_job = SD['plan_del_job']
    else:
        plan_del_job = plpy.prepare('delete from job_pool where wid=$1 and tgid=$2', ['integer', 'integer'])
        SD['plan_del_job'] = plan_del_job
    
    if 'plan_rem_jobs' in SD:
            plan_rem_jobs = SD['plan_rem_jobs']
    else:
        plan_rem_jobs = plpy.prepare('select 1 as found from job_pool where wid=$1 limit 1', ['integer'])
        SD['plan_rem_jobs'] = plan_rem_jobs
    
    if 'plan_add_trace' in SD:
        plan_add_trace = SD['plan_add_trace']
    else:
        plan_add_trace = plpy.prepare('insert into wed_trace (wid,trw,status,state) values ($1,$2,$3,$4)',['integer','text','text','json'])
        SD['plan_add_trace'] = plan_add_trace
        
    try:
        with plpy.subtransaction():
            plpy.execute(plan_del_job, [wid, tgid])
            #--remaning_jobs = plpy.execute(plan_rem_jobs, [wid])
            #--plpy.execute(plan_add_trace, [wid, ('_EOUT_' + job[0]['trname']), ('R' if remaning_jobs else 'E'), job[0]['payload']])
            
            #-- create an expection entry in job_pool for recovery purposes (THIS IS DONE IN KERNEL TRIGGER !)
            #--if not remaning_jobs:
            #--    if 'plan_end_inst' in SD:
            #--        plan_end_inst = SD['plan_end_inst']
            #--    else:
            #--        plan_end_inst = plpy.prepare('insert into job_pool (wid,tgid,trname,timeout,payload) values ($1,$2,$3,$4,$5)', ['integer','integer','text','interval','json'])
            #--        SD['plan_end_inst'] = plan_end_inst
            #--    
            #--    if 'plan_end_st' in SD:
            #--        plan_end_st = SD['plan_end_st']
            #--    else:
            #--        plan_end_st = plpy.prepare('update st_status set status=\'E\' where wid=$1', ['integer'])
            #--        SD['plan_end_st'] = plan_end_st
            #--    
            #--    plpy.execute(plan_end_inst, [wid, tgid, '_EXCPT', '1Y', job[0]['payload']])
            #--    plpy.execute(plan_end_st, [wid])

    except plpy.SPIError as e:
            plpy.error('TRSTOP ERROR: '+str(e))               
                
    return True
$$ LANGUAGE plpython3u SET search_path = 'public';
------------------------------------------------------------------------------------------------------------------------

--This is a control function to execute WED-transitions declared as stored procedures. It must receive an WED-state using
--the string representation of a python dictionary
CREATE OR REPLACE FUNCTION sp_worker (wed_state_text text, transition_name text, wid int, tgid int) RETURNS bool AS
$$  
    import json
    import time
      
    def dict_to_update(old_state, new_state):
        
        del(new_state['wid'])
        updated_attrs = {k:v for k,v in new_state.items() if new_state[k] != old_state[k]}
        return ','.join([key+'=\''+str(updated_attrs[key])+'\'' if updated_attrs[key] else key+'= NULL' for key in updated_attrs.keys()]) 
    
    def dict_to_row(old_wed_state_dict):
        
        attr_order = []
        attr_values = []
        
        #-- Get the order of columns in table wed_flow and save it in SD dictionary (Not necessary for python >= 3.7)
        #--TODO: Check if python version < 3.7. Otherwise, skip this part.
        if 'attr_order' in SD:
            attr_order = SD['attr_order']
            #-- plpy.info('AAA %s' % attr_order)
        else:
            try:
                res = plpy.execute('select a.attname, a.attnum from pg_attribute a join pg_class c on a.attrelid = c.oid where c.relname=\'wed_flow\' and a.attnum > 0 order by a.attnum')
            except plpy.SPIError as e:
                plpy.error(e)
            
            #--plpy.info('building a list of attributes in wed_flow table ...') 
            for i in res:
                attr_order.append(i['attname'])
            #-- plpy.info(attr_order)
            SD['attr_order'] = attr_order
        
        for attr in attr_order:
            attr_values.append(old_wed_state_dict[attr])
        
        #--plpy.info('VALUES: %s' % attr_values)
        return 'ROW(' + ','.join(['\''+str(x)+'\'' if x else 'NULL' for x in attr_values]) + ')'
    
    plpy.info(wed_state_text)
    try:
        with plpy.subtransaction():
            #--This lock will be granted once the SPI finishes the spawning transaction (i.e. the target job will be written on job_pool
            res = plpy.execute('select pg_advisory_xact_lock(%d, %d) as lock' % (wid, tgid))
            #--In case the spawning transaction aborts, the required job won't be in job pool
            job = plpy.execute('select trname, payload from job_pool where wid=%s and tgid=%s' % (wid, tgid))
            
            if (job and (job[0]['trname'] == transition_name)):
                plpy.log('Job found. Performing WED-transition %s' % (transition_name))
                
                old_wed_state_dict = json.loads(wed_state_text)
                #--payload in job_pool does not contain wid once it is not a wed-attribute
                old_wed_state_dict['wid'] = wid
                #--TODO: verify if function_name is in fact a wed_transition (receives and returns a wed-state)
                new_wed_state_dict = plpy.execute('select %s(%s)' % (transition_name, dict_to_row(old_wed_state_dict)))
                plpy.log('NEW STATE: %s' % new_wed_state_dict[0][transition_name])
                
                if (new_wed_state_dict and new_wed_state_dict[0][transition_name]):
                    update_str = dict_to_update(old_wed_state_dict, new_wed_state_dict[0][transition_name])
                    plpy.log('Updating instance %d with %s' % (wid, update_str))
                    try:
                        plpy.execute('update wed_flow set %s where wid=%d' % (update_str, wid))
                    except plpy.SPIError as e:
                        plpy.error('sp_worker update: %s' % str(e))
                    plpy.log('Instance %d updated!' % (wid))
                
                else:
                    plpy.error('WED-Transition %s did not return a valid WED-state' % (transition_name))
                    return False
            else:
                plpy.error('Job not found ! Aborting ...')
                return False
                
    except plpy.SPIError as e:
        plpy.error('sp_worker sub transaction: %s' % str(e))
    return True

$$ LANGUAGE plpython3u SET search_path = 'public';
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION enable_wed_trigger(tgname text) RETURNS bool AS
$$

    try:
        res = plpy.execute('update wed_trig set enabled = True where tgname=\''+tgname+'\' returning enabled')
    except plpy.SPIError as e:
        plpy.error(e)
    if not res:
        plpy.error('WED-trigger %s not found !' %(tgname))
    else:
        return res[0]['enabled']


$$ LANGUAGE plpython3u SET search_path = 'public';

CREATE OR REPLACE FUNCTION disable_wed_trigger(tgname text) RETURNS bool AS
$$

    try:
        res = plpy.execute('update wed_trig set enabled = False where tgname=\''+tgname+'\' returning enabled')
    except plpy.SPIError as e:
        plpy.error(e)
    if not res:
        plpy.error('WED-trigger %s not found !' %(tgname))
    else:
        return res[0]['enabled']


$$ LANGUAGE plpython3u SET search_path = 'public';
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION set_final_wed_state(predicate text) RETURNS void AS
$func$

    import re
            
    fbdtkn = re.compile(r'CREATE|DROP|ALTER|GRANT|REVOKE|SELECT|INSERT|UPDATE|DELETE|;',re.I)        
    found = fbdtkn.search(predicate)
    if found:
        plpy.error('Forbidden character or SQL keyword found in predicate: '+ found.group(0))
        #--return "SKIP"
    
    #--validate predicate
    try:
        res = plpy.execute('select True from wed_flow where '+predicate+ ' limit 1')
    except plpy.SPIError as e:
        plpy.error('Invalid predicate: %s' %(e))
    
    query = 'insert into wed_trig (tgname,trname,cname,cpred,trtout,trsp) values \
            (\'_FINAL\',\'_FINAL\',\'_FINAL\',$$'+predicate+'$$, NULL, \'f\')'
                          
    try:
        with plpy.subtransaction():
            plpy.execute('alter table wed_trig disable trigger wed_trig_insert_trg')
            res = plpy.execute(query)
            plpy.execute('alter table wed_trig enable trigger wed_trig_insert_trg')
    except plpy.SPIError as e:
        plpy.error('Could not set final WED-state: %s' %(e))
    
$func$ LANGUAGE plpython3u SECURITY DEFINER SET search_path = 'public';

--RESET ROLE; 
