import sys
from antlr4 import *

class WedflowSemantic():

    def __init__(self):
        self.condition_set = []
        self.trigger_def = {}
        self.trigger_set_name = ""
        self.flow_set = []

    def read_create_flow(self, ctx):
        tg_set_name = ctx.wed_flow_set_expr().SET_NAME().getText()
        conditions = ctx.wed_flow_set_expr().valid_name()
        self.flow_set.append(tg_set_name)
        self.flow_set.append(conditions[0].getText())
        self.flow_set.append(conditions[1].getText())

    def check_condition_set(self, ctx):
        condition_name_list = ctx.wc_set_expr().wc_valid_name()
        self.condition_set = [cond.getText() for cond in condition_name_list]
        defined_conditions = []
        error = ""
        condition_definition_list = ctx.condition_def()
        for condition_definition in condition_definition_list:
            condition_name = condition_definition.wc_valid_name().getText()
            defined_conditions.append(condition_name)
            if condition_name not in self.condition_set:
                error += "Error: Condition '{}' not declared in the condition set.\n".format(condition_name)
        for condition in self.condition_set:
            if condition not in defined_conditions:
                error += "Error: Condition '{}' declared but not defined.\n".format(condition)
        return error
            
    def check_trigger_set(self, ctx):
        trigger_name_list = ctx.set_expr().valid_name()
        self.trigger_set = [t.getText() for t in trigger_name_list]
        defined_triggers = []
        trigger_definition_list = ctx.trigger_def()
        self.trigger_set_name = ctx.SET_NAME().getText()
        error = ""
        for trigger_definition in trigger_definition_list:
            trigger_name = trigger_definition.valid_name()[0].getText().strip()
            condition_name = trigger_definition.valid_name()[1].getText().strip()
            transition_name = trigger_definition.valid_name()[2].getText().strip()
            defined_triggers.append(trigger_name)
            self.trigger_def[trigger_name] = {"condition" : condition_name, "transition" : transition_name}
            if trigger_name not in self.trigger_set:
                error += "Error: Trigger '{}' not declared in the trigger set.\n".format(trigger_name)
        for trigger in self.trigger_set:
            if trigger not in defined_triggers:
                error += "Error: Trigger '{}' declared but not defined.\n".format(trigger)
        return error

    def check_awic(self, final_condition):
        error = ""
        if final_condition not in self.condition_set:
            error += "Error: Condition '{}' in the final state is not declared in the condition set.\n".format(final_condition)
        return error

    def check_flow(self):
        error = ""
        if self.flow_set != []:
            if self.trigger_set_name != self.flow_set[0]:
                error += "Error: Diverging names for the trigger set in the trigger set declaration ({}) and the WED-flow set declaration ({}).\n".format(self.trigger_set_name, self.flow_set[0])
            if self.flow_set[1] not in self.condition_set:
                error += "Error: Condition '{}' from the WED-flow set declaration is not declared in the condition set.\n".format(self.flow_set[1])
            if self.flow_set[2] not in self.condition_set:
                error += "Error: Condition '{}' from the WED-flow set declaration is not declared in the condition set.\n".format(self.flow_set[2])
        return error

    def check_triggers(self, transition_set):
        error = ""
        for trigger_name, trigger in self.trigger_def.items():
            if trigger["condition"] not in self.condition_set:
                error += "Error: Condition '{}' from the trigger '{}' definiton is not declared in the condition set.\n".format(trigger["condition"], trigger_name)
            if trigger["transition"] not in transition_set:
                error += "Error: Transition '{}' from the trigger '{}' definiton is not declared in the transition set.\n".format(trigger["transition"], trigger_name)
        return error



