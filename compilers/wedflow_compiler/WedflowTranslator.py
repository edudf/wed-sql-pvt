import sys
from antlr4 import *

class WedflowTranslator():

    def __init__(self):
        self.cond_def = {}

    def create_flow(self, ctx):
        wedflow_name = ctx.valid_name().getText()
        wsql = "create flow {};\n".format(wedflow_name) + "\cw {};\n".format(wedflow_name) + "begin;\n" + "\n"
        return wsql 

    def create_attributes(self, ctx):
        attribute_name_list = ctx.set_expr().valid_name()
        wsql = ""
        for attribute_name in attribute_name_list:
            wsql += "create wed-attribute {};\n".format(attribute_name.getText())
        wsql += "\n"
        return wsql

    def prepare_conditions(self, ctx):
        condition_name_list = ctx.wc_set_expr().wc_valid_name()
        condition_definition_list = ctx.condition_def()
        for condition_definition in condition_definition_list:
            condition_name = condition_definition.wc_valid_name().getText()
            predicate = condition_definition.PREDICATE().getText()[1:].strip()
            self.cond_def[condition_name] = predicate
        return None
        
    def create_conditions(self, final_condition):
        wsql = ""
        for condition_name in self.cond_def.keys():
            if (condition_name != final_condition):
                wsql += "create wed-condition {} as {}\n".format(condition_name, self.cond_def[condition_name])
        wsql += "\n"
        return wsql

    def create_transitions(self, ctx):
        transition_name_list = ctx.set_expr().valid_name()
        wsql = ""
        for transition_name in transition_name_list:
            wsql += "create wed-transition {};\n".format(transition_name.getText())
        wsql += "\n"
        return wsql

    def create_triggers(self, ctx):
        trigger_name_list = ctx.set_expr().valid_name()
        trigger_definition_list = ctx.trigger_def()
        trigger_def = {}
        wsql = ""
        for trigger_definition in trigger_definition_list:
            trigger_name = trigger_definition.valid_name()[0].getText().strip()
            condition_name = trigger_definition.valid_name()[1].getText().strip()
            transition_name = trigger_definition.valid_name()[2].getText().strip()
            wsql += "create wed-trigger {} as ({}, {});\n".format(trigger_name, condition_name, transition_name)
        wsql += "\n"
        return wsql

    def set_final_state(self, final_condition):
        final_state_predicate = self.cond_def[final_condition][:-1]
        wsql = ""
        wsql += "set final wed-state as {};\n".format(final_state_predicate)    
        wsql += "\n"
        return wsql



