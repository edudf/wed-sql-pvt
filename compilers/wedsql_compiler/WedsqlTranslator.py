import sys
from antlr4 import *

class WedsqlTranslator():

    def __init__(self):
        self.cond_def = {}

    def create_attribute(self, ctx):
        stmt = "INSERT INTO wed_attr (aname, adv) VALUES (%s, %s);"
        attribute_name = ctx.valid_name().getText()
        default_value = ctx.opt_def_value_expr().value_expr()
        if default_value != None:
            default_value = default_value.getText()
        params = (attribute_name, default_value,)
        return (stmt, params)

    def create_condition(self, ctx):
        condition_expr = ctx.CONDITION_EXPR().getText().strip()
        condition_parts = condition_expr.split(None, 3)
        condition_name = condition_parts[1].strip()
        predicate = condition_parts[3][:-1].strip().replace("\n", "").replace("\r\n", "").replace("\t", "")
        stmt = "INSERT INTO wed_cond (cname,cpred) VALUES (%s, %s);"
        params = (condition_name, predicate,)
        return (stmt, params)

    def create_transition(self, ctx):
        transition_name = ctx.valid_name().getText()
        stop_expr = ctx.opt_stop_expr()
        proc_expr = ctx.opt_proc_expr()
        stored_proc = False
        if proc_expr.getText():
            stored_proc = True
        if stop_expr.getText():
            stop_cond_expr = stop_expr.stop_condition_expr()
            timeout_expr = stop_expr.timeout_expr()
            if stop_cond_expr != None:
                stop_condition = stop_cond_expr.valid_name().getText().strip()
                if timeout_expr != None:
                    timeout = timeout_expr.QUOTED_VALUE().getText().strip()
                    stmt = "INSERT INTO wed_trans (trname, trtout, trsc, trsp) VALUES (%s, %s, %s, %s);"
                    params = (transition_name, timeout, stop_condition, stored_proc,)
                else: 
                    stmt = "INSERT INTO wed_trans (trname, trsc, trsp) VALUES (%s, %s, %s);"
                    params = (transition_name, stop_condition, stored_proc,)
            else:
                if timeout_expr != None:
                    timeout = timeout_expr.QUOTED_VALUE().getText().strip()
                    stmt = "INSERT INTO wed_trans (trname, trtout, trsp) VALUES (%s, %s, %s);"
                    params = (transition_name, timeout, stored_proc,)
        else:
            stmt = "INSERT INTO wed_trans (trname, trsp) VALUES (%s, %s);"
            params = (transition_name, stored_proc,) 
        return (stmt, params)

    def create_trigger(self, ctx):
        trigger_id = ctx.valid_name().getText()
        trigger_expr = ctx.trigger_expr()
        cond_id = trigger_expr.valid_name(0).getText()
        trans_id = trigger_expr.valid_name(1).getText()
        stmt = "INSERT INTO wed_trig (tgname, cname, trname) VALUES (%s,%s,%s);"
        params = (trigger_id, cond_id, trans_id,) 
        return (stmt, params)

    def create_instance(self, ctx):
        value_list = ctx.inst_expr().value_list_expr()
        if (value_list == None):
            stmt = "INSERT INTO wed_flow DEFAULT VALUES;"
            params = None
        else:
            values = value_list.value_expr()
            if (ctx.inst_expr().attribute_list_expr() == None):
                stmt = "INSERT INTO wed_flow VALUES ("
            else:             
                columns = ctx.inst_expr().attribute_list_expr().getText().strip()
                stmt = "INSERT INTO wed_flow "+columns+" VALUES ("
            for i in range(len(values) - 1):
                values[i] = values[i].getText().strip()
                stmt += "%s, "
                if str(values[i])[0] == "'" and str(values[i])[-1] == "'":
                    values[i] = str(values[i])[1:-1]
            i = len(values) - 1
            values[i] = values[i].getText().strip()
            if str(values[i])[0] == "'" and str(values[i])[-1] == "'":
                values[i] = str(values[i])[1:-1]
            stmt += "%s);"
            params = tuple(values)
        return (stmt, params)

    def set_final_state(self, ctx):
        parts = ctx.SET_FINAL_STATE_EXPR().getText().lower().split(" as ", 1)
        predicate = ctx.SET_FINAL_STATE_EXPR().getText()[len(parts[0])+4:-1].strip()
        stmt = "SELECT set_final_wed_state(%s);"
        params = (predicate,)
        return (stmt, params)

    def enable_trigger(self, ctx):
        trigger_name = ctx.valid_name().getText()
        stmt = 'select enable_wed_trigger(%s) as enabled;'
        params = (trigger_name,)
        return (stmt, params)

    def disable_trigger(self, ctx):
        trigger_name = ctx.valid_name().getText()
        stmt = 'select disable_wed_trigger(%s) as enabled;'
        params = (trigger_name,)
        return (stmt, params)

    def list_attributes(self, ctx):
        attribute_name = ctx.valid_name()
        if attribute_name:
            stmt = 'select aname as "WED-attribute", coalesce(adv,\'NULL\') as "Default value" from wed_attr where aname = %s;'
            params = (attribute_name.getText(),)
        else:
            stmt = 'select aname as "WED-attribute", coalesce(adv,\'NULL\') as "Default value" from wed_attr;'
            params = None
        return (stmt, params)

    def list_conditions(self, ctx):
        condition_name = ctx.valid_name()
        if condition_name:
            stmt = 'select cname as "WED-condition", cpred as "Predicate" from wed_cond where cname = %s;'
            params = (condition_name.getText(),)
        else:
            stmt = 'select cname as "WED-condition", cpred as "Predicate" from wed_cond;'
            params = None
        return (stmt, params)

    def list_transitions(self, ctx):
        transition_name = ctx.valid_name()
        if transition_name:
            stmt = 'select trname as "WED-transition", trtout as "Timeout", coalesce(trsc,\'NULL\') as "Stop condition" from wed_trans where trname = %s;'
            params = (transition_name.getText(),)
        else:
            stmt = 'select trname as "WED-transition", trtout as "Timeout", coalesce(trsc,\'NULL\') as "Stop condition"  from wed_trans;'
            params = None
        return (stmt, params)

    def list_triggers(self, ctx):
        trigger_name = ctx.valid_name()
        if trigger_name:
            stmt = 'select tgname as "WED-trigger", cname as "WED-condition", trname as "WED-transition", enabled as "Enabled" from wed_trig where tgname = %s;'
            params = (trigger_name.getText(),)
        else:
            stmt = 'select tgname as "WED-trigger", cname as "WED-condition", trname as "WED-transition", enabled as "Enabled" from wed_trig where tgname <> %s;'
            params = ('_FINAL',)
        return (stmt, params)

    def list_fired_transitions(self, ctx, database):
        opt_id = ctx.opt_id().NAT_NUMBER()
        stmt = 'select j.wid as "Instance Id (wid)", t.tgname as "WED-trigger", j.trname as "WED-transition", j.timeout as "Timeout", (timeout - (now() - created)) as "Time left", j.payload as "WED-state", coalesce(lock.locked,\'f\') as Locked from job_pool as j left join (select True as locked,db.oid, db.datname, l.classid, l.objid from pg_database as db join pg_locks as l on db.oid = l.database where l.locktype=\'advisory\' and db.datname=%s) as lock on j.wid = lock.classid and j.tgid = lock.objid join wed_trig as t on j.tgid = t.tgid';
        if (opt_id == None):
            params = (database,)
        else:
            stmt = stmt + " where wid = %s"
            params = (database, opt_id.getText(),)
        stmt = stmt + ";"
        return (stmt, params)

    def list_instances(self, ctx):
        opt_id = ctx.opt_id()
        final = ctx.FINAL()
        transactioning = ctx.TRANSACTIONING()
        inconsistent = ctx.INCONSISTENT()
        status = None
        if (final != None):
            status = 'F'
        if (transactioning != None):
            status = 'R'
        if (inconsistent != None):
            status = 'E'
        stmt = 'select w.*, case when t1.status=\'R\' then \'Transactioning\' when t1.status=\'E\' then \'Inconsistent\' when t1.status=\'F\' then \'Final\' end as "Status" from wed_flow w join  wed_trace t1 on w.wid = t1.wid left join wed_trace t2  on (t1.wid = t2.wid and t1.tstmp < t2.tstmp) where t2.tstmp is null'
        params = None
        if status:
            stmt = stmt + " and t1.status = %s"
            params = (status,)
        if opt_id:
            stmt = stmt + " and w.wid = %s"
            if status:
                params = (status, opt_id.NAT_NUMBER().getText(),)
            else:
                params = (opt_id.NAT_NUMBER().getText(),)
        stmt = stmt + ";"
        return (stmt, params)

    def show_final_state(self, ctx):
        stmt = 'select cpred as "Final WED-state" from wed_trig where tgname=\'_FINAL\';'
        params = None
        return (stmt, params)

    def get_trace(self, ctx):
        wid = ctx.NAT_NUMBER().getText()
        stmt = 'select wid, trw as "Written by", trf as "Fired", case when status=\'R\' then \'Transactioning\' when status=\'E\' then \'Inconsistent\' when status=\'F\' then \'Final\' end as "Status", tstmp as "Timestamp", state as "WED-state" from wed_trace where wid= %s'
        params = (wid,)
        return (stmt, params)

    def terminate_instance(self, ctx):
        wid = ctx.NAT_NUMBER().getText()
        stmt = 'select terminate_instance(%s)';
        params = (wid,)
        return (stmt, params)

    def reinstate_instance(self, ctx):
        wid = ctx.NAT_NUMBER().getText()
        stmt = 'select reinstate_instance(%s)';
        params = (wid,)
        return (stmt, params)
