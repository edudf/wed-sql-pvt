# Generated from Wedsql.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .Wedsql import Wedsql
else:
    from Wedsql import Wedsql

# This class defines a complete listener for a parse tree produced by Wedsql.
class WedsqlListener(ParseTreeListener):

    # Enter a parse tree produced by Wedsql#wsql.
    def enterWsql(self, ctx:Wedsql.WsqlContext):
        pass

    # Exit a parse tree produced by Wedsql#wsql.
    def exitWsql(self, ctx:Wedsql.WsqlContext):
        pass


    # Enter a parse tree produced by Wedsql#stmt_list.
    def enterStmt_list(self, ctx:Wedsql.Stmt_listContext):
        pass

    # Exit a parse tree produced by Wedsql#stmt_list.
    def exitStmt_list(self, ctx:Wedsql.Stmt_listContext):
        pass


    # Enter a parse tree produced by Wedsql#stmt.
    def enterStmt(self, ctx:Wedsql.StmtContext):
        pass

    # Exit a parse tree produced by Wedsql#stmt.
    def exitStmt(self, ctx:Wedsql.StmtContext):
        pass


    # Enter a parse tree produced by Wedsql#create_flow_stmt.
    def enterCreate_flow_stmt(self, ctx:Wedsql.Create_flow_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#create_flow_stmt.
    def exitCreate_flow_stmt(self, ctx:Wedsql.Create_flow_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#remove_flow_stmt.
    def enterRemove_flow_stmt(self, ctx:Wedsql.Remove_flow_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#remove_flow_stmt.
    def exitRemove_flow_stmt(self, ctx:Wedsql.Remove_flow_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#create_attr_stmt.
    def enterCreate_attr_stmt(self, ctx:Wedsql.Create_attr_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#create_attr_stmt.
    def exitCreate_attr_stmt(self, ctx:Wedsql.Create_attr_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#create_cond_stmt.
    def enterCreate_cond_stmt(self, ctx:Wedsql.Create_cond_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#create_cond_stmt.
    def exitCreate_cond_stmt(self, ctx:Wedsql.Create_cond_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#create_trans_stmt.
    def enterCreate_trans_stmt(self, ctx:Wedsql.Create_trans_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#create_trans_stmt.
    def exitCreate_trans_stmt(self, ctx:Wedsql.Create_trans_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#create_trig_stmt.
    def enterCreate_trig_stmt(self, ctx:Wedsql.Create_trig_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#create_trig_stmt.
    def exitCreate_trig_stmt(self, ctx:Wedsql.Create_trig_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#create_inst_stmt.
    def enterCreate_inst_stmt(self, ctx:Wedsql.Create_inst_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#create_inst_stmt.
    def exitCreate_inst_stmt(self, ctx:Wedsql.Create_inst_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#set_fstate_stmt.
    def enterSet_fstate_stmt(self, ctx:Wedsql.Set_fstate_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#set_fstate_stmt.
    def exitSet_fstate_stmt(self, ctx:Wedsql.Set_fstate_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#enable_trig_stmt.
    def enterEnable_trig_stmt(self, ctx:Wedsql.Enable_trig_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#enable_trig_stmt.
    def exitEnable_trig_stmt(self, ctx:Wedsql.Enable_trig_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#disable_trig_stmt.
    def enterDisable_trig_stmt(self, ctx:Wedsql.Disable_trig_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#disable_trig_stmt.
    def exitDisable_trig_stmt(self, ctx:Wedsql.Disable_trig_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#list_attr_stmt.
    def enterList_attr_stmt(self, ctx:Wedsql.List_attr_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#list_attr_stmt.
    def exitList_attr_stmt(self, ctx:Wedsql.List_attr_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#list_cond_stmt.
    def enterList_cond_stmt(self, ctx:Wedsql.List_cond_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#list_cond_stmt.
    def exitList_cond_stmt(self, ctx:Wedsql.List_cond_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#list_trans_stmt.
    def enterList_trans_stmt(self, ctx:Wedsql.List_trans_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#list_trans_stmt.
    def exitList_trans_stmt(self, ctx:Wedsql.List_trans_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#list_trig_stmt.
    def enterList_trig_stmt(self, ctx:Wedsql.List_trig_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#list_trig_stmt.
    def exitList_trig_stmt(self, ctx:Wedsql.List_trig_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#list_ftrans_stmt.
    def enterList_ftrans_stmt(self, ctx:Wedsql.List_ftrans_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#list_ftrans_stmt.
    def exitList_ftrans_stmt(self, ctx:Wedsql.List_ftrans_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#list_insts_stmt.
    def enterList_insts_stmt(self, ctx:Wedsql.List_insts_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#list_insts_stmt.
    def exitList_insts_stmt(self, ctx:Wedsql.List_insts_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#show_fstate_stmt.
    def enterShow_fstate_stmt(self, ctx:Wedsql.Show_fstate_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#show_fstate_stmt.
    def exitShow_fstate_stmt(self, ctx:Wedsql.Show_fstate_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#get_trace_stmt.
    def enterGet_trace_stmt(self, ctx:Wedsql.Get_trace_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#get_trace_stmt.
    def exitGet_trace_stmt(self, ctx:Wedsql.Get_trace_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#terminate_stmt.
    def enterTerminate_stmt(self, ctx:Wedsql.Terminate_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#terminate_stmt.
    def exitTerminate_stmt(self, ctx:Wedsql.Terminate_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#reinstate_stmt.
    def enterReinstate_stmt(self, ctx:Wedsql.Reinstate_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#reinstate_stmt.
    def exitReinstate_stmt(self, ctx:Wedsql.Reinstate_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#begin_stmt.
    def enterBegin_stmt(self, ctx:Wedsql.Begin_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#begin_stmt.
    def exitBegin_stmt(self, ctx:Wedsql.Begin_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#commit_stmt.
    def enterCommit_stmt(self, ctx:Wedsql.Commit_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#commit_stmt.
    def exitCommit_stmt(self, ctx:Wedsql.Commit_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#abort_stmt.
    def enterAbort_stmt(self, ctx:Wedsql.Abort_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#abort_stmt.
    def exitAbort_stmt(self, ctx:Wedsql.Abort_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#shell_connect_flow_stmt.
    def enterShell_connect_flow_stmt(self, ctx:Wedsql.Shell_connect_flow_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#shell_connect_flow_stmt.
    def exitShell_connect_flow_stmt(self, ctx:Wedsql.Shell_connect_flow_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#shell_list_flow_stmt.
    def enterShell_list_flow_stmt(self, ctx:Wedsql.Shell_list_flow_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#shell_list_flow_stmt.
    def exitShell_list_flow_stmt(self, ctx:Wedsql.Shell_list_flow_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#shell_debug_msg_stmt.
    def enterShell_debug_msg_stmt(self, ctx:Wedsql.Shell_debug_msg_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#shell_debug_msg_stmt.
    def exitShell_debug_msg_stmt(self, ctx:Wedsql.Shell_debug_msg_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#shell_raw_output_stmt.
    def enterShell_raw_output_stmt(self, ctx:Wedsql.Shell_raw_output_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#shell_raw_output_stmt.
    def exitShell_raw_output_stmt(self, ctx:Wedsql.Shell_raw_output_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#shell_open_file_stmt.
    def enterShell_open_file_stmt(self, ctx:Wedsql.Shell_open_file_stmtContext):
        pass

    # Exit a parse tree produced by Wedsql#shell_open_file_stmt.
    def exitShell_open_file_stmt(self, ctx:Wedsql.Shell_open_file_stmtContext):
        pass


    # Enter a parse tree produced by Wedsql#opt_def_value_expr.
    def enterOpt_def_value_expr(self, ctx:Wedsql.Opt_def_value_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#opt_def_value_expr.
    def exitOpt_def_value_expr(self, ctx:Wedsql.Opt_def_value_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#value_expr.
    def enterValue_expr(self, ctx:Wedsql.Value_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#value_expr.
    def exitValue_expr(self, ctx:Wedsql.Value_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#opt_stop_expr.
    def enterOpt_stop_expr(self, ctx:Wedsql.Opt_stop_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#opt_stop_expr.
    def exitOpt_stop_expr(self, ctx:Wedsql.Opt_stop_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#timeout_expr.
    def enterTimeout_expr(self, ctx:Wedsql.Timeout_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#timeout_expr.
    def exitTimeout_expr(self, ctx:Wedsql.Timeout_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#stop_condition_expr.
    def enterStop_condition_expr(self, ctx:Wedsql.Stop_condition_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#stop_condition_expr.
    def exitStop_condition_expr(self, ctx:Wedsql.Stop_condition_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#opt_proc_expr.
    def enterOpt_proc_expr(self, ctx:Wedsql.Opt_proc_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#opt_proc_expr.
    def exitOpt_proc_expr(self, ctx:Wedsql.Opt_proc_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#trigger_expr.
    def enterTrigger_expr(self, ctx:Wedsql.Trigger_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#trigger_expr.
    def exitTrigger_expr(self, ctx:Wedsql.Trigger_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#inst_expr.
    def enterInst_expr(self, ctx:Wedsql.Inst_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#inst_expr.
    def exitInst_expr(self, ctx:Wedsql.Inst_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#value_list_expr.
    def enterValue_list_expr(self, ctx:Wedsql.Value_list_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#value_list_expr.
    def exitValue_list_expr(self, ctx:Wedsql.Value_list_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#attribute_list_expr.
    def enterAttribute_list_expr(self, ctx:Wedsql.Attribute_list_exprContext):
        pass

    # Exit a parse tree produced by Wedsql#attribute_list_expr.
    def exitAttribute_list_expr(self, ctx:Wedsql.Attribute_list_exprContext):
        pass


    # Enter a parse tree produced by Wedsql#opt_id.
    def enterOpt_id(self, ctx:Wedsql.Opt_idContext):
        pass

    # Exit a parse tree produced by Wedsql#opt_id.
    def exitOpt_id(self, ctx:Wedsql.Opt_idContext):
        pass


    # Enter a parse tree produced by Wedsql#valid_name.
    def enterValid_name(self, ctx:Wedsql.Valid_nameContext):
        pass

    # Exit a parse tree produced by Wedsql#valid_name.
    def exitValid_name(self, ctx:Wedsql.Valid_nameContext):
        pass


    # Enter a parse tree produced by Wedsql#reserved_words.
    def enterReserved_words(self, ctx:Wedsql.Reserved_wordsContext):
        pass

    # Exit a parse tree produced by Wedsql#reserved_words.
    def exitReserved_words(self, ctx:Wedsql.Reserved_wordsContext):
        pass


