from BaseWorker import BaseClass
import sys,random

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'verificar_material'
    dbs = 'user=processamento dbname=processamento application_name=ww-tr_ver'
    wakeup_interval = 50
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        if random.random() < 0.5:
            print("[\033[33mANALISE\033[0m] Material: %s, Condicao: \033[31mruim\033[0m] " % (payload['material']))
            cond = 'ruim'
        else:
            print("[\033[33mANALISE\033[0m] Material: %s, Condicao: \033[32mbom\033[0m] " % (payload['material']))
            cond = 'bom'
        
        return "condicao_material= '%s', mem = '%s'" % (cond, 'erro de procedimento na coleta' if cond == 'ruim' else '')
        #return None
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

