lexer grammar WedflowLexer;

ANNOTATION_WED_ATTRIBUTES : '@' [Ww] [Ee] [Dd] '-' [Aa] [Tt] [Tt] [Rr] [Ii] [Bb] [Uu] [Tt] [Ee] [Ss];

ANNOTATION_WED_CONDITIONS : '@' [Ww] [Ee] [Dd] '-' [Cc] [Oo] [Nn] [Dd] [Ii] [Tt] [Ii] [Oo] [Nn] [Ss] -> mode(WED_CONDITION);

ANNOTATION_WED_TRANSITIONS : '@' [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Aa] [Nn] [Ss] [Ii] [Tt] [Ii] [Oo] [Nn] [Ss];

ANNOTATION_WED_TRIGGERS : '@' [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Ii] [Gg] [Gg] [Ee] [Rr] [Ss];

ANNOTATION_WED_FLOW : '@' [Ww] [Ee] [Dd] '-' [Ff] [Ll] [Oo] [Ww];

SET_NAME : [A-Z] NON_SPECIAL*;

VALID_NAME : NON_SPECIAL+;

COLON : ':';

EQUALS : '=';

SEMICOLON : ';';

COMMA : ',';

OPEN_PARENTHESES : '(';

CLOSE_PARENTHESES : ')';

OPEN_CURLY_BRACKETS : '{';

CLOSE_CURLY_BRACKETS : '}';

LINE_COMMENT : ('//'|'--') .*? '\r'? '\n' -> skip;

fragment NON_SPECIAL : [a-zA-Z] | DIGIT | [\-_];

fragment DIGIT : [0-9];

SPACE : ' ';

LINEBREAK : '\r'? '\n';

TAB : [\t]+->skip;


mode WED_CONDITION;

WC_ANNOTATION_WED_ATTRIBUTES : '@' [Ww] [Ee] [Dd] '-' [Aa] [Tt] [Tt] [Rr] [Ii] [Bb] [Uu] [Tt] [Ee] [Ss] -> mode(DEFAULT_MODE);

WC_ANNOTATION_WED_TRANSITIONS : '@' [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Aa] [Nn] [Ss] [Ii] [Tt] [Ii] [Oo] [Nn] [Ss] -> mode(DEFAULT_MODE);

WC_ANNOTATION_WED_TRIGGERS : '@' [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Ii] [Gg] [Gg] [Ee] [Rr] [Ss] -> mode(DEFAULT_MODE);

WC_ANNOTATION_WED_FLOW : '@' [Ww] [Ee] [Dd] '-' [Ff] [Ll] [Oo] [Ww] -> mode(DEFAULT_MODE);

WC_SET_NAME : [A-Z] NON_SPECIAL*;

WC_VALID_NAME : NON_SPECIAL+;

WC_EQUALS : '=';

WC_SEMICOLON : ';';

WC_COMMA : ',';

WC_OPEN_PARENTHESES : '(';

WC_CLOSE_PARENTHESES : ')';

WC_OPEN_CURLY_BRACKETS : '{';

WC_CLOSE_CURLY_BRACKETS : '}';

WC_LINE_COMMENT : ('//'|'--') .*? '\r'? '\n' -> skip;

fragment WC_NON_SPECIAL : [a-zA-Z] | DIGIT | [\-_];

fragment WC_DIGIT : [0-9];

WC_SPACE : ' ';

WC_LINEBREAK : '\r'? '\n';

WC_TAB : [\t]+->skip;

PREDICATE : ':' .+? SEMICOLON;
