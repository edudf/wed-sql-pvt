from BaseWorker import BaseClass
import sys,psycopg2

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'iniciar_pfigado'
    dbs = 'user=coleta dbname=coleta application_name=ww-tr_pjx'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        self.new_processamento_instance(payload['bid'], 'figado')
        
        return "processamentos_iniciados = processamentos_iniciados::jsonb || '[\"figado\"]'::jsonb"
        #return None
    
    def new_processamento_instance(self,bid, material):
        try:
            conn = psycopg2.connect("dbname=processamento user=processamento")
        except Exception as e:
            print(e)
            exit(-1)
            
        cur = conn.cursor()
        try:
            cur.execute('insert into wed_flow (bid,material) values (%s,%s)', [bid,material])
        except Exception as e:
            print(e)
            exit(-2)
        
        print(cur.rowcount)
        conn.commit()
        conn.close()
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

