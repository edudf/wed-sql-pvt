from BaseWorker import BaseClass
import sys,random

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'aplicar_termos'
    dbs = 'user=entrevista dbname=entrevista application_name=ww-tr_term'
    wakeup_interval = 50
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        termos_consentidos = []
        termos = ['t_pjx', 't_pjk', 't_pjy', 't_pjz']
        
        for t in termos:
            if random.random() < 0.5:
                termos_consentidos.append(t)
                
        return "termos_consentidos='{%s}'" % (','.join(termos_consentidos))
        #return None
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

