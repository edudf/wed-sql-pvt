# Generated from Wedflow.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .Wedflow import Wedflow
else:
    from Wedflow import Wedflow

# This class defines a complete listener for a parse tree produced by Wedflow.
class WedflowListener(ParseTreeListener):

    # Enter a parse tree produced by Wedflow#wflow.
    def enterWflow(self, ctx:Wedflow.WflowContext):
        pass

    # Exit a parse tree produced by Wedflow#wflow.
    def exitWflow(self, ctx:Wedflow.WflowContext):
        pass


    # Enter a parse tree produced by Wedflow#stmt_list.
    def enterStmt_list(self, ctx:Wedflow.Stmt_listContext):
        pass

    # Exit a parse tree produced by Wedflow#stmt_list.
    def exitStmt_list(self, ctx:Wedflow.Stmt_listContext):
        pass


    # Enter a parse tree produced by Wedflow#stmt.
    def enterStmt(self, ctx:Wedflow.StmtContext):
        pass

    # Exit a parse tree produced by Wedflow#stmt.
    def exitStmt(self, ctx:Wedflow.StmtContext):
        pass


    # Enter a parse tree produced by Wedflow#create_flow_stmt.
    def enterCreate_flow_stmt(self, ctx:Wedflow.Create_flow_stmtContext):
        pass

    # Exit a parse tree produced by Wedflow#create_flow_stmt.
    def exitCreate_flow_stmt(self, ctx:Wedflow.Create_flow_stmtContext):
        pass


    # Enter a parse tree produced by Wedflow#create_attr_stmt.
    def enterCreate_attr_stmt(self, ctx:Wedflow.Create_attr_stmtContext):
        pass

    # Exit a parse tree produced by Wedflow#create_attr_stmt.
    def exitCreate_attr_stmt(self, ctx:Wedflow.Create_attr_stmtContext):
        pass


    # Enter a parse tree produced by Wedflow#create_cond_stmt.
    def enterCreate_cond_stmt(self, ctx:Wedflow.Create_cond_stmtContext):
        pass

    # Exit a parse tree produced by Wedflow#create_cond_stmt.
    def exitCreate_cond_stmt(self, ctx:Wedflow.Create_cond_stmtContext):
        pass


    # Enter a parse tree produced by Wedflow#create_trans_stmt.
    def enterCreate_trans_stmt(self, ctx:Wedflow.Create_trans_stmtContext):
        pass

    # Exit a parse tree produced by Wedflow#create_trans_stmt.
    def exitCreate_trans_stmt(self, ctx:Wedflow.Create_trans_stmtContext):
        pass


    # Enter a parse tree produced by Wedflow#create_trig_stmt.
    def enterCreate_trig_stmt(self, ctx:Wedflow.Create_trig_stmtContext):
        pass

    # Exit a parse tree produced by Wedflow#create_trig_stmt.
    def exitCreate_trig_stmt(self, ctx:Wedflow.Create_trig_stmtContext):
        pass


    # Enter a parse tree produced by Wedflow#annotation_wed_flow.
    def enterAnnotation_wed_flow(self, ctx:Wedflow.Annotation_wed_flowContext):
        pass

    # Exit a parse tree produced by Wedflow#annotation_wed_flow.
    def exitAnnotation_wed_flow(self, ctx:Wedflow.Annotation_wed_flowContext):
        pass


    # Enter a parse tree produced by Wedflow#annotation_wed_attributes.
    def enterAnnotation_wed_attributes(self, ctx:Wedflow.Annotation_wed_attributesContext):
        pass

    # Exit a parse tree produced by Wedflow#annotation_wed_attributes.
    def exitAnnotation_wed_attributes(self, ctx:Wedflow.Annotation_wed_attributesContext):
        pass


    # Enter a parse tree produced by Wedflow#annotation_wed_transitions.
    def enterAnnotation_wed_transitions(self, ctx:Wedflow.Annotation_wed_transitionsContext):
        pass

    # Exit a parse tree produced by Wedflow#annotation_wed_transitions.
    def exitAnnotation_wed_transitions(self, ctx:Wedflow.Annotation_wed_transitionsContext):
        pass


    # Enter a parse tree produced by Wedflow#annotation_wed_triggers.
    def enterAnnotation_wed_triggers(self, ctx:Wedflow.Annotation_wed_triggersContext):
        pass

    # Exit a parse tree produced by Wedflow#annotation_wed_triggers.
    def exitAnnotation_wed_triggers(self, ctx:Wedflow.Annotation_wed_triggersContext):
        pass


    # Enter a parse tree produced by Wedflow#set_expr.
    def enterSet_expr(self, ctx:Wedflow.Set_exprContext):
        pass

    # Exit a parse tree produced by Wedflow#set_expr.
    def exitSet_expr(self, ctx:Wedflow.Set_exprContext):
        pass


    # Enter a parse tree produced by Wedflow#wed_flow_set_expr.
    def enterWed_flow_set_expr(self, ctx:Wedflow.Wed_flow_set_exprContext):
        pass

    # Exit a parse tree produced by Wedflow#wed_flow_set_expr.
    def exitWed_flow_set_expr(self, ctx:Wedflow.Wed_flow_set_exprContext):
        pass


    # Enter a parse tree produced by Wedflow#trigger_def.
    def enterTrigger_def(self, ctx:Wedflow.Trigger_defContext):
        pass

    # Exit a parse tree produced by Wedflow#trigger_def.
    def exitTrigger_def(self, ctx:Wedflow.Trigger_defContext):
        pass


    # Enter a parse tree produced by Wedflow#valid_name.
    def enterValid_name(self, ctx:Wedflow.Valid_nameContext):
        pass

    # Exit a parse tree produced by Wedflow#valid_name.
    def exitValid_name(self, ctx:Wedflow.Valid_nameContext):
        pass


    # Enter a parse tree produced by Wedflow#empty_space.
    def enterEmpty_space(self, ctx:Wedflow.Empty_spaceContext):
        pass

    # Exit a parse tree produced by Wedflow#empty_space.
    def exitEmpty_space(self, ctx:Wedflow.Empty_spaceContext):
        pass


    # Enter a parse tree produced by Wedflow#wc_set_expr.
    def enterWc_set_expr(self, ctx:Wedflow.Wc_set_exprContext):
        pass

    # Exit a parse tree produced by Wedflow#wc_set_expr.
    def exitWc_set_expr(self, ctx:Wedflow.Wc_set_exprContext):
        pass


    # Enter a parse tree produced by Wedflow#condition_def.
    def enterCondition_def(self, ctx:Wedflow.Condition_defContext):
        pass

    # Exit a parse tree produced by Wedflow#condition_def.
    def exitCondition_def(self, ctx:Wedflow.Condition_defContext):
        pass


    # Enter a parse tree produced by Wedflow#wc_valid_name.
    def enterWc_valid_name(self, ctx:Wedflow.Wc_valid_nameContext):
        pass

    # Exit a parse tree produced by Wedflow#wc_valid_name.
    def exitWc_valid_name(self, ctx:Wedflow.Wc_valid_nameContext):
        pass


    # Enter a parse tree produced by Wedflow#wc_empty_space.
    def enterWc_empty_space(self, ctx:Wedflow.Wc_empty_spaceContext):
        pass

    # Exit a parse tree produced by Wedflow#wc_empty_space.
    def exitWc_empty_space(self, ctx:Wedflow.Wc_empty_spaceContext):
        pass


