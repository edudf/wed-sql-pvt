# Generated from Wedflow.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\'")
        buf.write("\u010e\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\5\38\n\3\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4L")
        buf.write("\n\4\3\5\3\5\7\5P\n\5\f\5\16\5S\13\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\6\3\6\7\6_\n\6\f\6\16\6b\13\6\3\6\3\6")
        buf.write("\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\7\7n\n\7\f\7\16\7q\13")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\7\7\u0080\n\7\f\7\16\7\u0083\13\7\3\b\3\b\7\b\u0087\n")
        buf.write("\b\f\b\16\b\u008a\13\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\t\3\t\7\t\u0097\n\t\f\t\16\t\u009a\13\t\3\t\3\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u00a9")
        buf.write("\n\t\f\t\16\t\u00ac\13\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r")
        buf.write("\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u00bd\n\16")
        buf.write("\f\16\16\16\u00c0\13\16\3\16\3\16\3\16\5\16\u00c5\n\16")
        buf.write("\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21")
        buf.write("\3\21\3\22\7\22\u00ea\n\22\f\22\16\22\u00ed\13\22\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u00f6\n\23\f\23\16")
        buf.write("\23\u00f9\13\23\3\23\3\23\3\23\5\23\u00fe\n\23\3\23\3")
        buf.write("\23\3\24\3\24\3\24\3\24\3\25\3\25\3\26\7\26\u0109\n\26")
        buf.write("\f\26\16\26\u010c\13\26\3\26\2\2\27\2\4\6\b\n\f\16\20")
        buf.write("\22\24\26\30\32\34\36 \"$&(*\2\n\4\2\7\7\31\31\4\2\3\3")
        buf.write("\26\26\4\2\5\5\27\27\4\2\6\6\30\30\3\2\b\t\3\2\23\24\3")
        buf.write("\2\32\33\3\2$%\2\u010a\2,\3\2\2\2\4\67\3\2\2\2\6K\3\2")
        buf.write("\2\2\bM\3\2\2\2\n\\\3\2\2\2\fk\3\2\2\2\16\u0084\3\2\2")
        buf.write("\2\20\u0094\3\2\2\2\22\u00ad\3\2\2\2\24\u00af\3\2\2\2")
        buf.write("\26\u00b1\3\2\2\2\30\u00b3\3\2\2\2\32\u00b5\3\2\2\2\34")
        buf.write("\u00c8\3\2\2\2\36\u00d6\3\2\2\2 \u00e6\3\2\2\2\"\u00eb")
        buf.write("\3\2\2\2$\u00ee\3\2\2\2&\u0101\3\2\2\2(\u0105\3\2\2\2")
        buf.write("*\u010a\3\2\2\2,-\5\4\3\2-\3\3\2\2\2./\5\"\22\2/\60\5")
        buf.write("\6\4\2\60\61\5\"\22\2\61\62\5\4\3\2\628\3\2\2\2\63\64")
        buf.write("\5\"\22\2\64\65\5\6\4\2\65\66\5\"\22\2\668\3\2\2\2\67")
        buf.write(".\3\2\2\2\67\63\3\2\2\28\5\3\2\2\29:\5\b\5\2:;\5\"\22")
        buf.write("\2;<\7\f\2\2<L\3\2\2\2=>\5\n\6\2>?\5\"\22\2?@\7\f\2\2")
        buf.write("@L\3\2\2\2AB\5\f\7\2BC\5\"\22\2CL\3\2\2\2DE\5\16\b\2E")
        buf.write("F\5\"\22\2FG\7\f\2\2GL\3\2\2\2HI\5\20\t\2IJ\5\"\22\2J")
        buf.write("L\3\2\2\2K9\3\2\2\2K=\3\2\2\2KA\3\2\2\2KD\3\2\2\2KH\3")
        buf.write("\2\2\2L\7\3\2\2\2MQ\5\22\n\2NP\7\23\2\2ON\3\2\2\2PS\3")
        buf.write("\2\2\2QO\3\2\2\2QR\3\2\2\2RT\3\2\2\2SQ\3\2\2\2TU\7\24")
        buf.write("\2\2UV\5\"\22\2VW\5 \21\2WX\5\"\22\2XY\7\13\2\2YZ\5\"")
        buf.write("\22\2Z[\5\34\17\2[\t\3\2\2\2\\`\5\24\13\2]_\7\23\2\2^")
        buf.write("]\3\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2ac\3\2\2\2b`\3")
        buf.write("\2\2\2cd\7\24\2\2de\5\"\22\2ef\7\b\2\2fg\5\"\22\2gh\7")
        buf.write("\13\2\2hi\5\"\22\2ij\5\32\16\2j\13\3\2\2\2ko\7\4\2\2l")
        buf.write("n\7$\2\2ml\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2pr\3\2")
        buf.write("\2\2qo\3\2\2\2rs\7%\2\2st\5*\26\2tu\7\32\2\2uv\5*\26\2")
        buf.write("vw\7\34\2\2wx\5*\26\2xy\5$\23\2yz\5*\26\2z{\7\35\2\2{")
        buf.write("\u0081\5*\26\2|}\5&\24\2}~\5*\26\2~\u0080\3\2\2\2\177")
        buf.write("|\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0081")
        buf.write("\u0082\3\2\2\2\u0082\r\3\2\2\2\u0083\u0081\3\2\2\2\u0084")
        buf.write("\u0088\5\26\f\2\u0085\u0087\7\23\2\2\u0086\u0085\3\2\2")
        buf.write("\2\u0087\u008a\3\2\2\2\u0088\u0086\3\2\2\2\u0088\u0089")
        buf.write("\3\2\2\2\u0089\u008b\3\2\2\2\u008a\u0088\3\2\2\2\u008b")
        buf.write("\u008c\7\24\2\2\u008c\u008d\5\"\22\2\u008d\u008e\7\b\2")
        buf.write("\2\u008e\u008f\5\"\22\2\u008f\u0090\7\13\2\2\u0090\u0091")
        buf.write("\5\"\22\2\u0091\u0092\5\32\16\2\u0092\u0093\5\"\22\2\u0093")
        buf.write("\17\3\2\2\2\u0094\u0098\5\30\r\2\u0095\u0097\7\23\2\2")
        buf.write("\u0096\u0095\3\2\2\2\u0097\u009a\3\2\2\2\u0098\u0096\3")
        buf.write("\2\2\2\u0098\u0099\3\2\2\2\u0099\u009b\3\2\2\2\u009a\u0098")
        buf.write("\3\2\2\2\u009b\u009c\7\24\2\2\u009c\u009d\5\"\22\2\u009d")
        buf.write("\u009e\7\b\2\2\u009e\u009f\5\"\22\2\u009f\u00a0\7\13\2")
        buf.write("\2\u00a0\u00a1\5\"\22\2\u00a1\u00a2\5\32\16\2\u00a2\u00a3")
        buf.write("\5\"\22\2\u00a3\u00a4\7\f\2\2\u00a4\u00aa\5\"\22\2\u00a5")
        buf.write("\u00a6\5\36\20\2\u00a6\u00a7\5\"\22\2\u00a7\u00a9\3\2")
        buf.write("\2\2\u00a8\u00a5\3\2\2\2\u00a9\u00ac\3\2\2\2\u00aa\u00a8")
        buf.write("\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\21\3\2\2\2\u00ac\u00aa")
        buf.write("\3\2\2\2\u00ad\u00ae\t\2\2\2\u00ae\23\3\2\2\2\u00af\u00b0")
        buf.write("\t\3\2\2\u00b0\25\3\2\2\2\u00b1\u00b2\t\4\2\2\u00b2\27")
        buf.write("\3\2\2\2\u00b3\u00b4\t\5\2\2\u00b4\31\3\2\2\2\u00b5\u00b6")
        buf.write("\7\20\2\2\u00b6\u00c4\5\"\22\2\u00b7\u00b8\5 \21\2\u00b8")
        buf.write("\u00b9\5\"\22\2\u00b9\u00ba\7\r\2\2\u00ba\u00bb\5\"\22")
        buf.write("\2\u00bb\u00bd\3\2\2\2\u00bc\u00b7\3\2\2\2\u00bd\u00c0")
        buf.write("\3\2\2\2\u00be\u00bc\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf")
        buf.write("\u00c1\3\2\2\2\u00c0\u00be\3\2\2\2\u00c1\u00c2\5 \21\2")
        buf.write("\u00c2\u00c3\5\"\22\2\u00c3\u00c5\3\2\2\2\u00c4\u00be")
        buf.write("\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c6")
        buf.write("\u00c7\7\21\2\2\u00c7\33\3\2\2\2\u00c8\u00c9\7\20\2\2")
        buf.write("\u00c9\u00ca\5\"\22\2\u00ca\u00cb\7\b\2\2\u00cb\u00cc")
        buf.write("\5\"\22\2\u00cc\u00cd\7\r\2\2\u00cd\u00ce\5\"\22\2\u00ce")
        buf.write("\u00cf\5 \21\2\u00cf\u00d0\5\"\22\2\u00d0\u00d1\7\r\2")
        buf.write("\2\u00d1\u00d2\5\"\22\2\u00d2\u00d3\5 \21\2\u00d3\u00d4")
        buf.write("\5\"\22\2\u00d4\u00d5\7\21\2\2\u00d5\35\3\2\2\2\u00d6")
        buf.write("\u00d7\5 \21\2\u00d7\u00d8\5\"\22\2\u00d8\u00d9\7\n\2")
        buf.write("\2\u00d9\u00da\5\"\22\2\u00da\u00db\7\16\2\2\u00db\u00dc")
        buf.write("\5\"\22\2\u00dc\u00dd\5 \21\2\u00dd\u00de\5\"\22\2\u00de")
        buf.write("\u00df\7\r\2\2\u00df\u00e0\5\"\22\2\u00e0\u00e1\5 \21")
        buf.write("\2\u00e1\u00e2\5\"\22\2\u00e2\u00e3\7\17\2\2\u00e3\u00e4")
        buf.write("\5\"\22\2\u00e4\u00e5\7\f\2\2\u00e5\37\3\2\2\2\u00e6\u00e7")
        buf.write("\t\6\2\2\u00e7!\3\2\2\2\u00e8\u00ea\t\7\2\2\u00e9\u00e8")
        buf.write("\3\2\2\2\u00ea\u00ed\3\2\2\2\u00eb\u00e9\3\2\2\2\u00eb")
        buf.write("\u00ec\3\2\2\2\u00ec#\3\2\2\2\u00ed\u00eb\3\2\2\2\u00ee")
        buf.write("\u00ef\7!\2\2\u00ef\u00fd\5*\26\2\u00f0\u00f1\5(\25\2")
        buf.write("\u00f1\u00f2\5*\26\2\u00f2\u00f3\7\36\2\2\u00f3\u00f4")
        buf.write("\5*\26\2\u00f4\u00f6\3\2\2\2\u00f5\u00f0\3\2\2\2\u00f6")
        buf.write("\u00f9\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f7\u00f8\3\2\2\2")
        buf.write("\u00f8\u00fa\3\2\2\2\u00f9\u00f7\3\2\2\2\u00fa\u00fb\5")
        buf.write("(\25\2\u00fb\u00fc\5*\26\2\u00fc\u00fe\3\2\2\2\u00fd\u00f7")
        buf.write("\3\2\2\2\u00fd\u00fe\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff")
        buf.write("\u0100\7\"\2\2\u0100%\3\2\2\2\u0101\u0102\5(\25\2\u0102")
        buf.write("\u0103\5*\26\2\u0103\u0104\7\'\2\2\u0104\'\3\2\2\2\u0105")
        buf.write("\u0106\t\b\2\2\u0106)\3\2\2\2\u0107\u0109\t\t\2\2\u0108")
        buf.write("\u0107\3\2\2\2\u0109\u010c\3\2\2\2\u010a\u0108\3\2\2\2")
        buf.write("\u010a\u010b\3\2\2\2\u010b+\3\2\2\2\u010c\u010a\3\2\2")
        buf.write("\2\21\67KQ`o\u0081\u0088\u0098\u00aa\u00be\u00c4\u00eb")
        buf.write("\u00f7\u00fd\u010a")
        return buf.getvalue()


class Wedflow ( Parser ):

    grammarFileName = "Wedflow.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "':'" ]

    symbolicNames = [ "<INVALID>", "ANNOTATION_WED_ATTRIBUTES", "ANNOTATION_WED_CONDITIONS", 
                      "ANNOTATION_WED_TRANSITIONS", "ANNOTATION_WED_TRIGGERS", 
                      "ANNOTATION_WED_FLOW", "SET_NAME", "VALID_NAME", "COLON", 
                      "EQUALS", "SEMICOLON", "COMMA", "OPEN_PARENTHESES", 
                      "CLOSE_PARENTHESES", "OPEN_CURLY_BRACKETS", "CLOSE_CURLY_BRACKETS", 
                      "LINE_COMMENT", "SPACE", "LINEBREAK", "TAB", "WC_ANNOTATION_WED_ATTRIBUTES", 
                      "WC_ANNOTATION_WED_TRANSITIONS", "WC_ANNOTATION_WED_TRIGGERS", 
                      "WC_ANNOTATION_WED_FLOW", "WC_SET_NAME", "WC_VALID_NAME", 
                      "WC_EQUALS", "WC_SEMICOLON", "WC_COMMA", "WC_OPEN_PARENTHESES", 
                      "WC_CLOSE_PARENTHESES", "WC_OPEN_CURLY_BRACKETS", 
                      "WC_CLOSE_CURLY_BRACKETS", "WC_LINE_COMMENT", "WC_SPACE", 
                      "WC_LINEBREAK", "WC_TAB", "PREDICATE" ]

    RULE_wflow = 0
    RULE_stmt_list = 1
    RULE_stmt = 2
    RULE_create_flow_stmt = 3
    RULE_create_attr_stmt = 4
    RULE_create_cond_stmt = 5
    RULE_create_trans_stmt = 6
    RULE_create_trig_stmt = 7
    RULE_annotation_wed_flow = 8
    RULE_annotation_wed_attributes = 9
    RULE_annotation_wed_transitions = 10
    RULE_annotation_wed_triggers = 11
    RULE_set_expr = 12
    RULE_wed_flow_set_expr = 13
    RULE_trigger_def = 14
    RULE_valid_name = 15
    RULE_empty_space = 16
    RULE_wc_set_expr = 17
    RULE_condition_def = 18
    RULE_wc_valid_name = 19
    RULE_wc_empty_space = 20

    ruleNames =  [ "wflow", "stmt_list", "stmt", "create_flow_stmt", "create_attr_stmt", 
                   "create_cond_stmt", "create_trans_stmt", "create_trig_stmt", 
                   "annotation_wed_flow", "annotation_wed_attributes", "annotation_wed_transitions", 
                   "annotation_wed_triggers", "set_expr", "wed_flow_set_expr", 
                   "trigger_def", "valid_name", "empty_space", "wc_set_expr", 
                   "condition_def", "wc_valid_name", "wc_empty_space" ]

    EOF = Token.EOF
    ANNOTATION_WED_ATTRIBUTES=1
    ANNOTATION_WED_CONDITIONS=2
    ANNOTATION_WED_TRANSITIONS=3
    ANNOTATION_WED_TRIGGERS=4
    ANNOTATION_WED_FLOW=5
    SET_NAME=6
    VALID_NAME=7
    COLON=8
    EQUALS=9
    SEMICOLON=10
    COMMA=11
    OPEN_PARENTHESES=12
    CLOSE_PARENTHESES=13
    OPEN_CURLY_BRACKETS=14
    CLOSE_CURLY_BRACKETS=15
    LINE_COMMENT=16
    SPACE=17
    LINEBREAK=18
    TAB=19
    WC_ANNOTATION_WED_ATTRIBUTES=20
    WC_ANNOTATION_WED_TRANSITIONS=21
    WC_ANNOTATION_WED_TRIGGERS=22
    WC_ANNOTATION_WED_FLOW=23
    WC_SET_NAME=24
    WC_VALID_NAME=25
    WC_EQUALS=26
    WC_SEMICOLON=27
    WC_COMMA=28
    WC_OPEN_PARENTHESES=29
    WC_CLOSE_PARENTHESES=30
    WC_OPEN_CURLY_BRACKETS=31
    WC_CLOSE_CURLY_BRACKETS=32
    WC_LINE_COMMENT=33
    WC_SPACE=34
    WC_LINEBREAK=35
    WC_TAB=36
    PREDICATE=37

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class WflowContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stmt_list(self):
            return self.getTypedRuleContext(Wedflow.Stmt_listContext,0)


        def getRuleIndex(self):
            return Wedflow.RULE_wflow

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWflow" ):
                listener.enterWflow(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWflow" ):
                listener.exitWflow(self)




    def wflow(self):

        localctx = Wedflow.WflowContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_wflow)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 42
            self.stmt_list()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Stmt_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def stmt(self):
            return self.getTypedRuleContext(Wedflow.StmtContext,0)


        def stmt_list(self):
            return self.getTypedRuleContext(Wedflow.Stmt_listContext,0)


        def getRuleIndex(self):
            return Wedflow.RULE_stmt_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStmt_list" ):
                listener.enterStmt_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStmt_list" ):
                listener.exitStmt_list(self)




    def stmt_list(self):

        localctx = Wedflow.Stmt_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_stmt_list)
        try:
            self.state = 53
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 44
                self.empty_space()
                self.state = 45
                self.stmt()
                self.state = 46
                self.empty_space()
                self.state = 47
                self.stmt_list()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 49
                self.empty_space()
                self.state = 50
                self.stmt()
                self.state = 51
                self.empty_space()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def create_flow_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_flow_stmtContext,0)


        def empty_space(self):
            return self.getTypedRuleContext(Wedflow.Empty_spaceContext,0)


        def SEMICOLON(self):
            return self.getToken(Wedflow.SEMICOLON, 0)

        def create_attr_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_attr_stmtContext,0)


        def create_cond_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_cond_stmtContext,0)


        def create_trans_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_trans_stmtContext,0)


        def create_trig_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_trig_stmtContext,0)


        def getRuleIndex(self):
            return Wedflow.RULE_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStmt" ):
                listener.enterStmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStmt" ):
                listener.exitStmt(self)




    def stmt(self):

        localctx = Wedflow.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_stmt)
        try:
            self.state = 73
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [Wedflow.ANNOTATION_WED_FLOW, Wedflow.WC_ANNOTATION_WED_FLOW]:
                self.enterOuterAlt(localctx, 1)
                self.state = 55
                self.create_flow_stmt()
                self.state = 56
                self.empty_space()
                self.state = 57
                self.match(Wedflow.SEMICOLON)
                pass
            elif token in [Wedflow.ANNOTATION_WED_ATTRIBUTES, Wedflow.WC_ANNOTATION_WED_ATTRIBUTES]:
                self.enterOuterAlt(localctx, 2)
                self.state = 59
                self.create_attr_stmt()
                self.state = 60
                self.empty_space()
                self.state = 61
                self.match(Wedflow.SEMICOLON)
                pass
            elif token in [Wedflow.ANNOTATION_WED_CONDITIONS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 63
                self.create_cond_stmt()
                self.state = 64
                self.empty_space()
                pass
            elif token in [Wedflow.ANNOTATION_WED_TRANSITIONS, Wedflow.WC_ANNOTATION_WED_TRANSITIONS]:
                self.enterOuterAlt(localctx, 4)
                self.state = 66
                self.create_trans_stmt()
                self.state = 67
                self.empty_space()
                self.state = 68
                self.match(Wedflow.SEMICOLON)
                pass
            elif token in [Wedflow.ANNOTATION_WED_TRIGGERS, Wedflow.WC_ANNOTATION_WED_TRIGGERS]:
                self.enterOuterAlt(localctx, 5)
                self.state = 70
                self.create_trig_stmt()
                self.state = 71
                self.empty_space()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_flow_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation_wed_flow(self):
            return self.getTypedRuleContext(Wedflow.Annotation_wed_flowContext,0)


        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def valid_name(self):
            return self.getTypedRuleContext(Wedflow.Valid_nameContext,0)


        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def wed_flow_set_expr(self):
            return self.getTypedRuleContext(Wedflow.Wed_flow_set_exprContext,0)


        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def getRuleIndex(self):
            return Wedflow.RULE_create_flow_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_flow_stmt" ):
                listener.enterCreate_flow_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_flow_stmt" ):
                listener.exitCreate_flow_stmt(self)




    def create_flow_stmt(self):

        localctx = Wedflow.Create_flow_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_create_flow_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 75
            self.annotation_wed_flow()
            self.state = 79
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 76
                self.match(Wedflow.SPACE)
                self.state = 81
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 82
            self.match(Wedflow.LINEBREAK)
            self.state = 83
            self.empty_space()
            self.state = 84
            self.valid_name()
            self.state = 85
            self.empty_space()
            self.state = 86
            self.match(Wedflow.EQUALS)
            self.state = 87
            self.empty_space()
            self.state = 88
            self.wed_flow_set_expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_attr_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation_wed_attributes(self):
            return self.getTypedRuleContext(Wedflow.Annotation_wed_attributesContext,0)


        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def getRuleIndex(self):
            return Wedflow.RULE_create_attr_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_attr_stmt" ):
                listener.enterCreate_attr_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_attr_stmt" ):
                listener.exitCreate_attr_stmt(self)




    def create_attr_stmt(self):

        localctx = Wedflow.Create_attr_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_create_attr_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            self.annotation_wed_attributes()
            self.state = 94
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 91
                self.match(Wedflow.SPACE)
                self.state = 96
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 97
            self.match(Wedflow.LINEBREAK)
            self.state = 98
            self.empty_space()
            self.state = 99
            self.match(Wedflow.SET_NAME)
            self.state = 100
            self.empty_space()
            self.state = 101
            self.match(Wedflow.EQUALS)
            self.state = 102
            self.empty_space()
            self.state = 103
            self.set_expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_cond_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANNOTATION_WED_CONDITIONS(self):
            return self.getToken(Wedflow.ANNOTATION_WED_CONDITIONS, 0)

        def WC_LINEBREAK(self):
            return self.getToken(Wedflow.WC_LINEBREAK, 0)

        def wc_empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Wc_empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Wc_empty_spaceContext,i)


        def WC_SET_NAME(self):
            return self.getToken(Wedflow.WC_SET_NAME, 0)

        def WC_EQUALS(self):
            return self.getToken(Wedflow.WC_EQUALS, 0)

        def wc_set_expr(self):
            return self.getTypedRuleContext(Wedflow.Wc_set_exprContext,0)


        def WC_SEMICOLON(self):
            return self.getToken(Wedflow.WC_SEMICOLON, 0)

        def WC_SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.WC_SPACE)
            else:
                return self.getToken(Wedflow.WC_SPACE, i)

        def condition_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Condition_defContext)
            else:
                return self.getTypedRuleContext(Wedflow.Condition_defContext,i)


        def getRuleIndex(self):
            return Wedflow.RULE_create_cond_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_cond_stmt" ):
                listener.enterCreate_cond_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_cond_stmt" ):
                listener.exitCreate_cond_stmt(self)




    def create_cond_stmt(self):

        localctx = Wedflow.Create_cond_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_create_cond_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self.match(Wedflow.ANNOTATION_WED_CONDITIONS)
            self.state = 109
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.WC_SPACE:
                self.state = 106
                self.match(Wedflow.WC_SPACE)
                self.state = 111
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 112
            self.match(Wedflow.WC_LINEBREAK)
            self.state = 113
            self.wc_empty_space()
            self.state = 114
            self.match(Wedflow.WC_SET_NAME)
            self.state = 115
            self.wc_empty_space()
            self.state = 116
            self.match(Wedflow.WC_EQUALS)
            self.state = 117
            self.wc_empty_space()
            self.state = 118
            self.wc_set_expr()
            self.state = 119
            self.wc_empty_space()
            self.state = 120
            self.match(Wedflow.WC_SEMICOLON)
            self.state = 121
            self.wc_empty_space()
            self.state = 127
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.WC_SET_NAME or _la==Wedflow.WC_VALID_NAME:
                self.state = 122
                self.condition_def()
                self.state = 123
                self.wc_empty_space()
                self.state = 129
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_trans_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation_wed_transitions(self):
            return self.getTypedRuleContext(Wedflow.Annotation_wed_transitionsContext,0)


        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def getRuleIndex(self):
            return Wedflow.RULE_create_trans_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_trans_stmt" ):
                listener.enterCreate_trans_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_trans_stmt" ):
                listener.exitCreate_trans_stmt(self)




    def create_trans_stmt(self):

        localctx = Wedflow.Create_trans_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_create_trans_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130
            self.annotation_wed_transitions()
            self.state = 134
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 131
                self.match(Wedflow.SPACE)
                self.state = 136
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 137
            self.match(Wedflow.LINEBREAK)
            self.state = 138
            self.empty_space()
            self.state = 139
            self.match(Wedflow.SET_NAME)
            self.state = 140
            self.empty_space()
            self.state = 141
            self.match(Wedflow.EQUALS)
            self.state = 142
            self.empty_space()
            self.state = 143
            self.set_expr()
            self.state = 144
            self.empty_space()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_trig_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def annotation_wed_triggers(self):
            return self.getTypedRuleContext(Wedflow.Annotation_wed_triggersContext,0)


        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SEMICOLON(self):
            return self.getToken(Wedflow.SEMICOLON, 0)

        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def trigger_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Trigger_defContext)
            else:
                return self.getTypedRuleContext(Wedflow.Trigger_defContext,i)


        def getRuleIndex(self):
            return Wedflow.RULE_create_trig_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_trig_stmt" ):
                listener.enterCreate_trig_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_trig_stmt" ):
                listener.exitCreate_trig_stmt(self)




    def create_trig_stmt(self):

        localctx = Wedflow.Create_trig_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_create_trig_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 146
            self.annotation_wed_triggers()
            self.state = 150
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 147
                self.match(Wedflow.SPACE)
                self.state = 152
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 153
            self.match(Wedflow.LINEBREAK)
            self.state = 154
            self.empty_space()
            self.state = 155
            self.match(Wedflow.SET_NAME)
            self.state = 156
            self.empty_space()
            self.state = 157
            self.match(Wedflow.EQUALS)
            self.state = 158
            self.empty_space()
            self.state = 159
            self.set_expr()
            self.state = 160
            self.empty_space()
            self.state = 161
            self.match(Wedflow.SEMICOLON)
            self.state = 162
            self.empty_space()
            self.state = 168
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SET_NAME or _la==Wedflow.VALID_NAME:
                self.state = 163
                self.trigger_def()
                self.state = 164
                self.empty_space()
                self.state = 170
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Annotation_wed_flowContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WC_ANNOTATION_WED_FLOW(self):
            return self.getToken(Wedflow.WC_ANNOTATION_WED_FLOW, 0)

        def ANNOTATION_WED_FLOW(self):
            return self.getToken(Wedflow.ANNOTATION_WED_FLOW, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_annotation_wed_flow

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotation_wed_flow" ):
                listener.enterAnnotation_wed_flow(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotation_wed_flow" ):
                listener.exitAnnotation_wed_flow(self)




    def annotation_wed_flow(self):

        localctx = Wedflow.Annotation_wed_flowContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_annotation_wed_flow)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 171
            _la = self._input.LA(1)
            if not(_la==Wedflow.ANNOTATION_WED_FLOW or _la==Wedflow.WC_ANNOTATION_WED_FLOW):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Annotation_wed_attributesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WC_ANNOTATION_WED_ATTRIBUTES(self):
            return self.getToken(Wedflow.WC_ANNOTATION_WED_ATTRIBUTES, 0)

        def ANNOTATION_WED_ATTRIBUTES(self):
            return self.getToken(Wedflow.ANNOTATION_WED_ATTRIBUTES, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_annotation_wed_attributes

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotation_wed_attributes" ):
                listener.enterAnnotation_wed_attributes(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotation_wed_attributes" ):
                listener.exitAnnotation_wed_attributes(self)




    def annotation_wed_attributes(self):

        localctx = Wedflow.Annotation_wed_attributesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_annotation_wed_attributes)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 173
            _la = self._input.LA(1)
            if not(_la==Wedflow.ANNOTATION_WED_ATTRIBUTES or _la==Wedflow.WC_ANNOTATION_WED_ATTRIBUTES):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Annotation_wed_transitionsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WC_ANNOTATION_WED_TRANSITIONS(self):
            return self.getToken(Wedflow.WC_ANNOTATION_WED_TRANSITIONS, 0)

        def ANNOTATION_WED_TRANSITIONS(self):
            return self.getToken(Wedflow.ANNOTATION_WED_TRANSITIONS, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_annotation_wed_transitions

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotation_wed_transitions" ):
                listener.enterAnnotation_wed_transitions(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotation_wed_transitions" ):
                listener.exitAnnotation_wed_transitions(self)




    def annotation_wed_transitions(self):

        localctx = Wedflow.Annotation_wed_transitionsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_annotation_wed_transitions)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 175
            _la = self._input.LA(1)
            if not(_la==Wedflow.ANNOTATION_WED_TRANSITIONS or _la==Wedflow.WC_ANNOTATION_WED_TRANSITIONS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Annotation_wed_triggersContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WC_ANNOTATION_WED_TRIGGERS(self):
            return self.getToken(Wedflow.WC_ANNOTATION_WED_TRIGGERS, 0)

        def ANNOTATION_WED_TRIGGERS(self):
            return self.getToken(Wedflow.ANNOTATION_WED_TRIGGERS, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_annotation_wed_triggers

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnnotation_wed_triggers" ):
                listener.enterAnnotation_wed_triggers(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnnotation_wed_triggers" ):
                listener.exitAnnotation_wed_triggers(self)




    def annotation_wed_triggers(self):

        localctx = Wedflow.Annotation_wed_triggersContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_annotation_wed_triggers)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 177
            _la = self._input.LA(1)
            if not(_la==Wedflow.ANNOTATION_WED_TRIGGERS or _la==Wedflow.WC_ANNOTATION_WED_TRIGGERS):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Set_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OPEN_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.OPEN_CURLY_BRACKETS, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def CLOSE_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.CLOSE_CURLY_BRACKETS, 0)

        def valid_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Valid_nameContext)
            else:
                return self.getTypedRuleContext(Wedflow.Valid_nameContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.COMMA)
            else:
                return self.getToken(Wedflow.COMMA, i)

        def getRuleIndex(self):
            return Wedflow.RULE_set_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSet_expr" ):
                listener.enterSet_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSet_expr" ):
                listener.exitSet_expr(self)




    def set_expr(self):

        localctx = Wedflow.Set_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_set_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 179
            self.match(Wedflow.OPEN_CURLY_BRACKETS)
            self.state = 180
            self.empty_space()
            self.state = 194
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==Wedflow.SET_NAME or _la==Wedflow.VALID_NAME:
                self.state = 188
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 181
                        self.valid_name()
                        self.state = 182
                        self.empty_space()
                        self.state = 183
                        self.match(Wedflow.COMMA)
                        self.state = 184
                        self.empty_space() 
                    self.state = 190
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,9,self._ctx)

                self.state = 191
                self.valid_name()
                self.state = 192
                self.empty_space()


            self.state = 196
            self.match(Wedflow.CLOSE_CURLY_BRACKETS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Wed_flow_set_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OPEN_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.OPEN_CURLY_BRACKETS, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.COMMA)
            else:
                return self.getToken(Wedflow.COMMA, i)

        def valid_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Valid_nameContext)
            else:
                return self.getTypedRuleContext(Wedflow.Valid_nameContext,i)


        def CLOSE_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.CLOSE_CURLY_BRACKETS, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_wed_flow_set_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWed_flow_set_expr" ):
                listener.enterWed_flow_set_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWed_flow_set_expr" ):
                listener.exitWed_flow_set_expr(self)




    def wed_flow_set_expr(self):

        localctx = Wedflow.Wed_flow_set_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_wed_flow_set_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 198
            self.match(Wedflow.OPEN_CURLY_BRACKETS)
            self.state = 199
            self.empty_space()
            self.state = 200
            self.match(Wedflow.SET_NAME)
            self.state = 201
            self.empty_space()
            self.state = 202
            self.match(Wedflow.COMMA)
            self.state = 203
            self.empty_space()
            self.state = 204
            self.valid_name()
            self.state = 205
            self.empty_space()
            self.state = 206
            self.match(Wedflow.COMMA)
            self.state = 207
            self.empty_space()
            self.state = 208
            self.valid_name()
            self.state = 209
            self.empty_space()
            self.state = 210
            self.match(Wedflow.CLOSE_CURLY_BRACKETS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Trigger_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def valid_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Valid_nameContext)
            else:
                return self.getTypedRuleContext(Wedflow.Valid_nameContext,i)


        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def COLON(self):
            return self.getToken(Wedflow.COLON, 0)

        def OPEN_PARENTHESES(self):
            return self.getToken(Wedflow.OPEN_PARENTHESES, 0)

        def COMMA(self):
            return self.getToken(Wedflow.COMMA, 0)

        def CLOSE_PARENTHESES(self):
            return self.getToken(Wedflow.CLOSE_PARENTHESES, 0)

        def SEMICOLON(self):
            return self.getToken(Wedflow.SEMICOLON, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_trigger_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTrigger_def" ):
                listener.enterTrigger_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTrigger_def" ):
                listener.exitTrigger_def(self)




    def trigger_def(self):

        localctx = Wedflow.Trigger_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_trigger_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 212
            self.valid_name()
            self.state = 213
            self.empty_space()
            self.state = 214
            self.match(Wedflow.COLON)
            self.state = 215
            self.empty_space()
            self.state = 216
            self.match(Wedflow.OPEN_PARENTHESES)
            self.state = 217
            self.empty_space()
            self.state = 218
            self.valid_name()
            self.state = 219
            self.empty_space()
            self.state = 220
            self.match(Wedflow.COMMA)
            self.state = 221
            self.empty_space()
            self.state = 222
            self.valid_name()
            self.state = 223
            self.empty_space()
            self.state = 224
            self.match(Wedflow.CLOSE_PARENTHESES)
            self.state = 225
            self.empty_space()
            self.state = 226
            self.match(Wedflow.SEMICOLON)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Valid_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def VALID_NAME(self):
            return self.getToken(Wedflow.VALID_NAME, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_valid_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterValid_name" ):
                listener.enterValid_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitValid_name" ):
                listener.exitValid_name(self)




    def valid_name(self):

        localctx = Wedflow.Valid_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_valid_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 228
            _la = self._input.LA(1)
            if not(_la==Wedflow.SET_NAME or _la==Wedflow.VALID_NAME):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Empty_spaceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def LINEBREAK(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.LINEBREAK)
            else:
                return self.getToken(Wedflow.LINEBREAK, i)

        def getRuleIndex(self):
            return Wedflow.RULE_empty_space

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEmpty_space" ):
                listener.enterEmpty_space(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEmpty_space" ):
                listener.exitEmpty_space(self)




    def empty_space(self):

        localctx = Wedflow.Empty_spaceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_empty_space)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 233
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,11,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 230
                    _la = self._input.LA(1)
                    if not(_la==Wedflow.SPACE or _la==Wedflow.LINEBREAK):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume() 
                self.state = 235
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,11,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Wc_set_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WC_OPEN_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.WC_OPEN_CURLY_BRACKETS, 0)

        def wc_empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Wc_empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Wc_empty_spaceContext,i)


        def WC_CLOSE_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.WC_CLOSE_CURLY_BRACKETS, 0)

        def wc_valid_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Wc_valid_nameContext)
            else:
                return self.getTypedRuleContext(Wedflow.Wc_valid_nameContext,i)


        def WC_COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.WC_COMMA)
            else:
                return self.getToken(Wedflow.WC_COMMA, i)

        def getRuleIndex(self):
            return Wedflow.RULE_wc_set_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWc_set_expr" ):
                listener.enterWc_set_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWc_set_expr" ):
                listener.exitWc_set_expr(self)




    def wc_set_expr(self):

        localctx = Wedflow.Wc_set_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_wc_set_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 236
            self.match(Wedflow.WC_OPEN_CURLY_BRACKETS)
            self.state = 237
            self.wc_empty_space()
            self.state = 251
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==Wedflow.WC_SET_NAME or _la==Wedflow.WC_VALID_NAME:
                self.state = 245
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,12,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 238
                        self.wc_valid_name()
                        self.state = 239
                        self.wc_empty_space()
                        self.state = 240
                        self.match(Wedflow.WC_COMMA)
                        self.state = 241
                        self.wc_empty_space() 
                    self.state = 247
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,12,self._ctx)

                self.state = 248
                self.wc_valid_name()
                self.state = 249
                self.wc_empty_space()


            self.state = 253
            self.match(Wedflow.WC_CLOSE_CURLY_BRACKETS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Condition_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def wc_valid_name(self):
            return self.getTypedRuleContext(Wedflow.Wc_valid_nameContext,0)


        def wc_empty_space(self):
            return self.getTypedRuleContext(Wedflow.Wc_empty_spaceContext,0)


        def PREDICATE(self):
            return self.getToken(Wedflow.PREDICATE, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_condition_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCondition_def" ):
                listener.enterCondition_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCondition_def" ):
                listener.exitCondition_def(self)




    def condition_def(self):

        localctx = Wedflow.Condition_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_condition_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 255
            self.wc_valid_name()
            self.state = 256
            self.wc_empty_space()
            self.state = 257
            self.match(Wedflow.PREDICATE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Wc_valid_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WC_SET_NAME(self):
            return self.getToken(Wedflow.WC_SET_NAME, 0)

        def WC_VALID_NAME(self):
            return self.getToken(Wedflow.WC_VALID_NAME, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_wc_valid_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWc_valid_name" ):
                listener.enterWc_valid_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWc_valid_name" ):
                listener.exitWc_valid_name(self)




    def wc_valid_name(self):

        localctx = Wedflow.Wc_valid_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_wc_valid_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 259
            _la = self._input.LA(1)
            if not(_la==Wedflow.WC_SET_NAME or _la==Wedflow.WC_VALID_NAME):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Wc_empty_spaceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def WC_SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.WC_SPACE)
            else:
                return self.getToken(Wedflow.WC_SPACE, i)

        def WC_LINEBREAK(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.WC_LINEBREAK)
            else:
                return self.getToken(Wedflow.WC_LINEBREAK, i)

        def getRuleIndex(self):
            return Wedflow.RULE_wc_empty_space

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWc_empty_space" ):
                listener.enterWc_empty_space(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWc_empty_space" ):
                listener.exitWc_empty_space(self)




    def wc_empty_space(self):

        localctx = Wedflow.Wc_empty_spaceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_wc_empty_space)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 264
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.WC_SPACE or _la==Wedflow.WC_LINEBREAK:
                self.state = 261
                _la = self._input.LA(1)
                if not(_la==Wedflow.WC_SPACE or _la==Wedflow.WC_LINEBREAK):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 266
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





