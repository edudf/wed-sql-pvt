# Generated from WedflowLexer.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\'")
        buf.write("\u0170\b\1\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6")
        buf.write("\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r")
        buf.write("\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write("\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30")
        buf.write("\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35")
        buf.write("\4\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4")
        buf.write("%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\3\2\3\2\3\2\3\2\3")
        buf.write("\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6")
        buf.write("\3\6\3\6\3\6\3\7\3\7\7\7\u00a4\n\7\f\7\16\7\u00a7\13\7")
        buf.write("\3\b\6\b\u00aa\n\b\r\b\16\b\u00ab\3\t\3\t\3\n\3\n\3\13")
        buf.write("\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3")
        buf.write("\21\3\21\3\21\3\21\5\21\u00c2\n\21\3\21\7\21\u00c5\n\21")
        buf.write("\f\21\16\21\u00c8\13\21\3\21\5\21\u00cb\n\21\3\21\3\21")
        buf.write("\3\21\3\21\3\22\3\22\3\22\5\22\u00d4\n\22\3\23\3\23\3")
        buf.write("\24\3\24\3\25\5\25\u00db\n\25\3\25\3\25\3\26\6\26\u00e0")
        buf.write("\n\26\r\26\16\26\u00e1\3\26\3\26\3\27\3\27\3\27\3\27\3")
        buf.write("\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\7\33\u0129\n")
        buf.write("\33\f\33\16\33\u012c\13\33\3\34\6\34\u012f\n\34\r\34\16")
        buf.write("\34\u0130\3\35\3\35\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3")
        buf.write("\"\3\"\3#\3#\3$\3$\3$\3$\5$\u0145\n$\3$\7$\u0148\n$\f")
        buf.write("$\16$\u014b\13$\3$\5$\u014e\n$\3$\3$\3$\3$\3%\3%\3%\5")
        buf.write("%\u0157\n%\3&\3&\3\'\3\'\3(\5(\u015e\n(\3(\3(\3)\6)\u0163")
        buf.write("\n)\r)\16)\u0164\3)\3)\3*\3*\6*\u016b\n*\r*\16*\u016c")
        buf.write("\3*\3*\5\u00c6\u0149\u016c\2+\4\3\6\4\b\5\n\6\f\7\16\b")
        buf.write("\20\t\22\n\24\13\26\f\30\r\32\16\34\17\36\20 \21\"\22")
        buf.write("$\2&\2(\23*\24,\25.\26\60\27\62\30\64\31\66\328\33:\34")
        buf.write("<\35>\36@\37B D!F\"H#J\2L\2N$P%R&T\'\4\2\3\27\4\2YYyy")
        buf.write("\4\2GGgg\4\2FFff\4\2CCcc\4\2VVvv\4\2TTtt\4\2KKkk\4\2D")
        buf.write("Ddd\4\2WWww\4\2UUuu\4\2EEee\4\2QQqq\4\2PPpp\4\2IIii\4")
        buf.write("\2HHhh\4\2NNnn\3\2C\\\4\2C\\c|\4\2//aa\3\2\62;\3\2\13")
        buf.write("\13\2\u017d\2\4\3\2\2\2\2\6\3\2\2\2\2\b\3\2\2\2\2\n\3")
        buf.write("\2\2\2\2\f\3\2\2\2\2\16\3\2\2\2\2\20\3\2\2\2\2\22\3\2")
        buf.write("\2\2\2\24\3\2\2\2\2\26\3\2\2\2\2\30\3\2\2\2\2\32\3\2\2")
        buf.write("\2\2\34\3\2\2\2\2\36\3\2\2\2\2 \3\2\2\2\2\"\3\2\2\2\2")
        buf.write("(\3\2\2\2\2*\3\2\2\2\2,\3\2\2\2\3.\3\2\2\2\3\60\3\2\2")
        buf.write("\2\3\62\3\2\2\2\3\64\3\2\2\2\3\66\3\2\2\2\38\3\2\2\2\3")
        buf.write(":\3\2\2\2\3<\3\2\2\2\3>\3\2\2\2\3@\3\2\2\2\3B\3\2\2\2")
        buf.write("\3D\3\2\2\2\3F\3\2\2\2\3H\3\2\2\2\3N\3\2\2\2\3P\3\2\2")
        buf.write("\2\3R\3\2\2\2\3T\3\2\2\2\4V\3\2\2\2\6f\3\2\2\2\bx\3\2")
        buf.write("\2\2\n\u0089\3\2\2\2\f\u0097\3\2\2\2\16\u00a1\3\2\2\2")
        buf.write("\20\u00a9\3\2\2\2\22\u00ad\3\2\2\2\24\u00af\3\2\2\2\26")
        buf.write("\u00b1\3\2\2\2\30\u00b3\3\2\2\2\32\u00b5\3\2\2\2\34\u00b7")
        buf.write("\3\2\2\2\36\u00b9\3\2\2\2 \u00bb\3\2\2\2\"\u00c1\3\2\2")
        buf.write("\2$\u00d3\3\2\2\2&\u00d5\3\2\2\2(\u00d7\3\2\2\2*\u00da")
        buf.write("\3\2\2\2,\u00df\3\2\2\2.\u00e5\3\2\2\2\60\u00f7\3\2\2")
        buf.write("\2\62\u010a\3\2\2\2\64\u011a\3\2\2\2\66\u0126\3\2\2\2")
        buf.write("8\u012e\3\2\2\2:\u0132\3\2\2\2<\u0134\3\2\2\2>\u0136\3")
        buf.write("\2\2\2@\u0138\3\2\2\2B\u013a\3\2\2\2D\u013c\3\2\2\2F\u013e")
        buf.write("\3\2\2\2H\u0144\3\2\2\2J\u0156\3\2\2\2L\u0158\3\2\2\2")
        buf.write("N\u015a\3\2\2\2P\u015d\3\2\2\2R\u0162\3\2\2\2T\u0168\3")
        buf.write("\2\2\2VW\7B\2\2WX\t\2\2\2XY\t\3\2\2YZ\t\4\2\2Z[\7/\2\2")
        buf.write("[\\\t\5\2\2\\]\t\6\2\2]^\t\6\2\2^_\t\7\2\2_`\t\b\2\2`")
        buf.write("a\t\t\2\2ab\t\n\2\2bc\t\6\2\2cd\t\3\2\2de\t\13\2\2e\5")
        buf.write("\3\2\2\2fg\7B\2\2gh\t\2\2\2hi\t\3\2\2ij\t\4\2\2jk\7/\2")
        buf.write("\2kl\t\f\2\2lm\t\r\2\2mn\t\16\2\2no\t\4\2\2op\t\b\2\2")
        buf.write("pq\t\6\2\2qr\t\b\2\2rs\t\r\2\2st\t\16\2\2tu\t\13\2\2u")
        buf.write("v\3\2\2\2vw\b\3\2\2w\7\3\2\2\2xy\7B\2\2yz\t\2\2\2z{\t")
        buf.write("\3\2\2{|\t\4\2\2|}\7/\2\2}~\t\6\2\2~\177\t\7\2\2\177\u0080")
        buf.write("\t\5\2\2\u0080\u0081\t\16\2\2\u0081\u0082\t\13\2\2\u0082")
        buf.write("\u0083\t\b\2\2\u0083\u0084\t\6\2\2\u0084\u0085\t\b\2\2")
        buf.write("\u0085\u0086\t\r\2\2\u0086\u0087\t\16\2\2\u0087\u0088")
        buf.write("\t\13\2\2\u0088\t\3\2\2\2\u0089\u008a\7B\2\2\u008a\u008b")
        buf.write("\t\2\2\2\u008b\u008c\t\3\2\2\u008c\u008d\t\4\2\2\u008d")
        buf.write("\u008e\7/\2\2\u008e\u008f\t\6\2\2\u008f\u0090\t\7\2\2")
        buf.write("\u0090\u0091\t\b\2\2\u0091\u0092\t\17\2\2\u0092\u0093")
        buf.write("\t\17\2\2\u0093\u0094\t\3\2\2\u0094\u0095\t\7\2\2\u0095")
        buf.write("\u0096\t\13\2\2\u0096\13\3\2\2\2\u0097\u0098\7B\2\2\u0098")
        buf.write("\u0099\t\2\2\2\u0099\u009a\t\3\2\2\u009a\u009b\t\4\2\2")
        buf.write("\u009b\u009c\7/\2\2\u009c\u009d\t\20\2\2\u009d\u009e\t")
        buf.write("\21\2\2\u009e\u009f\t\r\2\2\u009f\u00a0\t\2\2\2\u00a0")
        buf.write("\r\3\2\2\2\u00a1\u00a5\t\22\2\2\u00a2\u00a4\5$\22\2\u00a3")
        buf.write("\u00a2\3\2\2\2\u00a4\u00a7\3\2\2\2\u00a5\u00a3\3\2\2\2")
        buf.write("\u00a5\u00a6\3\2\2\2\u00a6\17\3\2\2\2\u00a7\u00a5\3\2")
        buf.write("\2\2\u00a8\u00aa\5$\22\2\u00a9\u00a8\3\2\2\2\u00aa\u00ab")
        buf.write("\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac")
        buf.write("\21\3\2\2\2\u00ad\u00ae\7<\2\2\u00ae\23\3\2\2\2\u00af")
        buf.write("\u00b0\7?\2\2\u00b0\25\3\2\2\2\u00b1\u00b2\7=\2\2\u00b2")
        buf.write("\27\3\2\2\2\u00b3\u00b4\7.\2\2\u00b4\31\3\2\2\2\u00b5")
        buf.write("\u00b6\7*\2\2\u00b6\33\3\2\2\2\u00b7\u00b8\7+\2\2\u00b8")
        buf.write("\35\3\2\2\2\u00b9\u00ba\7}\2\2\u00ba\37\3\2\2\2\u00bb")
        buf.write("\u00bc\7\177\2\2\u00bc!\3\2\2\2\u00bd\u00be\7\61\2\2\u00be")
        buf.write("\u00c2\7\61\2\2\u00bf\u00c0\7/\2\2\u00c0\u00c2\7/\2\2")
        buf.write("\u00c1\u00bd\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c2\u00c6\3")
        buf.write("\2\2\2\u00c3\u00c5\13\2\2\2\u00c4\u00c3\3\2\2\2\u00c5")
        buf.write("\u00c8\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c6\u00c4\3\2\2\2")
        buf.write("\u00c7\u00ca\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c9\u00cb\7")
        buf.write("\17\2\2\u00ca\u00c9\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb")
        buf.write("\u00cc\3\2\2\2\u00cc\u00cd\7\f\2\2\u00cd\u00ce\3\2\2\2")
        buf.write("\u00ce\u00cf\b\21\3\2\u00cf#\3\2\2\2\u00d0\u00d4\t\23")
        buf.write("\2\2\u00d1\u00d4\5&\23\2\u00d2\u00d4\t\24\2\2\u00d3\u00d0")
        buf.write("\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3\u00d2\3\2\2\2\u00d4")
        buf.write("%\3\2\2\2\u00d5\u00d6\t\25\2\2\u00d6\'\3\2\2\2\u00d7\u00d8")
        buf.write("\7\"\2\2\u00d8)\3\2\2\2\u00d9\u00db\7\17\2\2\u00da\u00d9")
        buf.write("\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc")
        buf.write("\u00dd\7\f\2\2\u00dd+\3\2\2\2\u00de\u00e0\t\26\2\2\u00df")
        buf.write("\u00de\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00df\3\2\2\2")
        buf.write("\u00e1\u00e2\3\2\2\2\u00e2\u00e3\3\2\2\2\u00e3\u00e4\b")
        buf.write("\26\3\2\u00e4-\3\2\2\2\u00e5\u00e6\7B\2\2\u00e6\u00e7")
        buf.write("\t\2\2\2\u00e7\u00e8\t\3\2\2\u00e8\u00e9\t\4\2\2\u00e9")
        buf.write("\u00ea\7/\2\2\u00ea\u00eb\t\5\2\2\u00eb\u00ec\t\6\2\2")
        buf.write("\u00ec\u00ed\t\6\2\2\u00ed\u00ee\t\7\2\2\u00ee\u00ef\t")
        buf.write("\b\2\2\u00ef\u00f0\t\t\2\2\u00f0\u00f1\t\n\2\2\u00f1\u00f2")
        buf.write("\t\6\2\2\u00f2\u00f3\t\3\2\2\u00f3\u00f4\t\13\2\2\u00f4")
        buf.write("\u00f5\3\2\2\2\u00f5\u00f6\b\27\4\2\u00f6/\3\2\2\2\u00f7")
        buf.write("\u00f8\7B\2\2\u00f8\u00f9\t\2\2\2\u00f9\u00fa\t\3\2\2")
        buf.write("\u00fa\u00fb\t\4\2\2\u00fb\u00fc\7/\2\2\u00fc\u00fd\t")
        buf.write("\6\2\2\u00fd\u00fe\t\7\2\2\u00fe\u00ff\t\5\2\2\u00ff\u0100")
        buf.write("\t\16\2\2\u0100\u0101\t\13\2\2\u0101\u0102\t\b\2\2\u0102")
        buf.write("\u0103\t\6\2\2\u0103\u0104\t\b\2\2\u0104\u0105\t\r\2\2")
        buf.write("\u0105\u0106\t\16\2\2\u0106\u0107\t\13\2\2\u0107\u0108")
        buf.write("\3\2\2\2\u0108\u0109\b\30\4\2\u0109\61\3\2\2\2\u010a\u010b")
        buf.write("\7B\2\2\u010b\u010c\t\2\2\2\u010c\u010d\t\3\2\2\u010d")
        buf.write("\u010e\t\4\2\2\u010e\u010f\7/\2\2\u010f\u0110\t\6\2\2")
        buf.write("\u0110\u0111\t\7\2\2\u0111\u0112\t\b\2\2\u0112\u0113\t")
        buf.write("\17\2\2\u0113\u0114\t\17\2\2\u0114\u0115\t\3\2\2\u0115")
        buf.write("\u0116\t\7\2\2\u0116\u0117\t\13\2\2\u0117\u0118\3\2\2")
        buf.write("\2\u0118\u0119\b\31\4\2\u0119\63\3\2\2\2\u011a\u011b\7")
        buf.write("B\2\2\u011b\u011c\t\2\2\2\u011c\u011d\t\3\2\2\u011d\u011e")
        buf.write("\t\4\2\2\u011e\u011f\7/\2\2\u011f\u0120\t\20\2\2\u0120")
        buf.write("\u0121\t\21\2\2\u0121\u0122\t\r\2\2\u0122\u0123\t\2\2")
        buf.write("\2\u0123\u0124\3\2\2\2\u0124\u0125\b\32\4\2\u0125\65\3")
        buf.write("\2\2\2\u0126\u012a\t\22\2\2\u0127\u0129\5$\22\2\u0128")
        buf.write("\u0127\3\2\2\2\u0129\u012c\3\2\2\2\u012a\u0128\3\2\2\2")
        buf.write("\u012a\u012b\3\2\2\2\u012b\67\3\2\2\2\u012c\u012a\3\2")
        buf.write("\2\2\u012d\u012f\5$\22\2\u012e\u012d\3\2\2\2\u012f\u0130")
        buf.write("\3\2\2\2\u0130\u012e\3\2\2\2\u0130\u0131\3\2\2\2\u0131")
        buf.write("9\3\2\2\2\u0132\u0133\7?\2\2\u0133;\3\2\2\2\u0134\u0135")
        buf.write("\7=\2\2\u0135=\3\2\2\2\u0136\u0137\7.\2\2\u0137?\3\2\2")
        buf.write("\2\u0138\u0139\7*\2\2\u0139A\3\2\2\2\u013a\u013b\7+\2")
        buf.write("\2\u013bC\3\2\2\2\u013c\u013d\7}\2\2\u013dE\3\2\2\2\u013e")
        buf.write("\u013f\7\177\2\2\u013fG\3\2\2\2\u0140\u0141\7\61\2\2\u0141")
        buf.write("\u0145\7\61\2\2\u0142\u0143\7/\2\2\u0143\u0145\7/\2\2")
        buf.write("\u0144\u0140\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u0149\3")
        buf.write("\2\2\2\u0146\u0148\13\2\2\2\u0147\u0146\3\2\2\2\u0148")
        buf.write("\u014b\3\2\2\2\u0149\u014a\3\2\2\2\u0149\u0147\3\2\2\2")
        buf.write("\u014a\u014d\3\2\2\2\u014b\u0149\3\2\2\2\u014c\u014e\7")
        buf.write("\17\2\2\u014d\u014c\3\2\2\2\u014d\u014e\3\2\2\2\u014e")
        buf.write("\u014f\3\2\2\2\u014f\u0150\7\f\2\2\u0150\u0151\3\2\2\2")
        buf.write("\u0151\u0152\b$\3\2\u0152I\3\2\2\2\u0153\u0157\t\23\2")
        buf.write("\2\u0154\u0157\5&\23\2\u0155\u0157\t\24\2\2\u0156\u0153")
        buf.write("\3\2\2\2\u0156\u0154\3\2\2\2\u0156\u0155\3\2\2\2\u0157")
        buf.write("K\3\2\2\2\u0158\u0159\t\25\2\2\u0159M\3\2\2\2\u015a\u015b")
        buf.write("\7\"\2\2\u015bO\3\2\2\2\u015c\u015e\7\17\2\2\u015d\u015c")
        buf.write("\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u015f\3\2\2\2\u015f")
        buf.write("\u0160\7\f\2\2\u0160Q\3\2\2\2\u0161\u0163\t\26\2\2\u0162")
        buf.write("\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0162\3\2\2\2")
        buf.write("\u0164\u0165\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u0167\b")
        buf.write(")\3\2\u0167S\3\2\2\2\u0168\u016a\7<\2\2\u0169\u016b\13")
        buf.write("\2\2\2\u016a\u0169\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u016d")
        buf.write("\3\2\2\2\u016c\u016a\3\2\2\2\u016d\u016e\3\2\2\2\u016e")
        buf.write("\u016f\5\26\13\2\u016fU\3\2\2\2\25\2\3\u00a5\u00ab\u00c1")
        buf.write("\u00c6\u00ca\u00d3\u00da\u00e1\u012a\u0130\u0144\u0149")
        buf.write("\u014d\u0156\u015d\u0164\u016c\5\4\3\2\b\2\2\4\2\2")
        return buf.getvalue()


class WedflowLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    WED_CONDITION = 1

    ANNOTATION_WED_ATTRIBUTES = 1
    ANNOTATION_WED_CONDITIONS = 2
    ANNOTATION_WED_TRANSITIONS = 3
    ANNOTATION_WED_TRIGGERS = 4
    ANNOTATION_WED_FLOW = 5
    SET_NAME = 6
    VALID_NAME = 7
    COLON = 8
    EQUALS = 9
    SEMICOLON = 10
    COMMA = 11
    OPEN_PARENTHESES = 12
    CLOSE_PARENTHESES = 13
    OPEN_CURLY_BRACKETS = 14
    CLOSE_CURLY_BRACKETS = 15
    LINE_COMMENT = 16
    SPACE = 17
    LINEBREAK = 18
    TAB = 19
    WC_ANNOTATION_WED_ATTRIBUTES = 20
    WC_ANNOTATION_WED_TRANSITIONS = 21
    WC_ANNOTATION_WED_TRIGGERS = 22
    WC_ANNOTATION_WED_FLOW = 23
    WC_SET_NAME = 24
    WC_VALID_NAME = 25
    WC_EQUALS = 26
    WC_SEMICOLON = 27
    WC_COMMA = 28
    WC_OPEN_PARENTHESES = 29
    WC_CLOSE_PARENTHESES = 30
    WC_OPEN_CURLY_BRACKETS = 31
    WC_CLOSE_CURLY_BRACKETS = 32
    WC_LINE_COMMENT = 33
    WC_SPACE = 34
    WC_LINEBREAK = 35
    WC_TAB = 36
    PREDICATE = 37

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE", "WED_CONDITION" ]

    literalNames = [ "<INVALID>",
            "':'" ]

    symbolicNames = [ "<INVALID>",
            "ANNOTATION_WED_ATTRIBUTES", "ANNOTATION_WED_CONDITIONS", "ANNOTATION_WED_TRANSITIONS", 
            "ANNOTATION_WED_TRIGGERS", "ANNOTATION_WED_FLOW", "SET_NAME", 
            "VALID_NAME", "COLON", "EQUALS", "SEMICOLON", "COMMA", "OPEN_PARENTHESES", 
            "CLOSE_PARENTHESES", "OPEN_CURLY_BRACKETS", "CLOSE_CURLY_BRACKETS", 
            "LINE_COMMENT", "SPACE", "LINEBREAK", "TAB", "WC_ANNOTATION_WED_ATTRIBUTES", 
            "WC_ANNOTATION_WED_TRANSITIONS", "WC_ANNOTATION_WED_TRIGGERS", 
            "WC_ANNOTATION_WED_FLOW", "WC_SET_NAME", "WC_VALID_NAME", "WC_EQUALS", 
            "WC_SEMICOLON", "WC_COMMA", "WC_OPEN_PARENTHESES", "WC_CLOSE_PARENTHESES", 
            "WC_OPEN_CURLY_BRACKETS", "WC_CLOSE_CURLY_BRACKETS", "WC_LINE_COMMENT", 
            "WC_SPACE", "WC_LINEBREAK", "WC_TAB", "PREDICATE" ]

    ruleNames = [ "ANNOTATION_WED_ATTRIBUTES", "ANNOTATION_WED_CONDITIONS", 
                  "ANNOTATION_WED_TRANSITIONS", "ANNOTATION_WED_TRIGGERS", 
                  "ANNOTATION_WED_FLOW", "SET_NAME", "VALID_NAME", "COLON", 
                  "EQUALS", "SEMICOLON", "COMMA", "OPEN_PARENTHESES", "CLOSE_PARENTHESES", 
                  "OPEN_CURLY_BRACKETS", "CLOSE_CURLY_BRACKETS", "LINE_COMMENT", 
                  "NON_SPECIAL", "DIGIT", "SPACE", "LINEBREAK", "TAB", "WC_ANNOTATION_WED_ATTRIBUTES", 
                  "WC_ANNOTATION_WED_TRANSITIONS", "WC_ANNOTATION_WED_TRIGGERS", 
                  "WC_ANNOTATION_WED_FLOW", "WC_SET_NAME", "WC_VALID_NAME", 
                  "WC_EQUALS", "WC_SEMICOLON", "WC_COMMA", "WC_OPEN_PARENTHESES", 
                  "WC_CLOSE_PARENTHESES", "WC_OPEN_CURLY_BRACKETS", "WC_CLOSE_CURLY_BRACKETS", 
                  "WC_LINE_COMMENT", "WC_NON_SPECIAL", "WC_DIGIT", "WC_SPACE", 
                  "WC_LINEBREAK", "WC_TAB", "PREDICATE" ]

    grammarFileName = "WedflowLexer.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


