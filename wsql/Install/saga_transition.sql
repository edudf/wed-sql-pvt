create or replace function saga_transition(wid int, trname text)
returns void as
$body$
    import json
    import time
    
    try:
        trg_id = plpy.execute("select tgid from wed_trig where trname = '%s'" % (trname))
        tgid = trg_id[0]['tgid']
    except Exception as e:
        plpy.error('WED_TRANSITION %s NOT FOUND !' % (trname))
    
    plpy.info('TGID %d' % (tgid))
    plpy.info('ACQUIRING LOCK ...')
    try:
        locked = plpy.execute('select pg_try_advisory_xact_lock(%d,%d) as lock' % (wid, tgid))
    except Exception as e:
        plpy.error('LOCK ACQUIRING ERROR !')
    
    if locked[0]['lock']:
        job = plpy.execute('select TRUE as found, payload as input_state from job_pool where wid=%d and tgid=%d' % (wid, tgid))
        if job and job[0]['found']:
            plpy.info('LOCK ACQUIRED.')
            plpy.info('PERFORMING WED-TRANSITION: %s' % (trname))
            
            try:
                output_state = plpy.execute("select %s('%s'::json)" % (trname, json.dumps(job[0]['input_state'])))
            except Exception as e:
                plpy.error('ERROR WHEN RUNNING %s' % (trname), e)
            
            if output_state:
                plpy.info('%s RETURNED: %s' % (trname, output_state[0][trname]))
                plpy.info('UPDATING INSTANCE %d' % (wid))
                output_json = json.loads(output_state[0][trname])
                upd_expr = ', '.join([k+' = \''+str(output_json[k])+'\'' for k in output_json.keys()])
                
                try:
                    plpy.execute('update wed_flow set ' + upd_expr + ' where wid=%d' % (wid))
                except Exception as e:
                    plpy.error('UPDATE ERROR !', e)
                else:
                    plpy.info('TRASACTION COMMITED.')
                
            else:
                plpy.error('%s RETURNED AN EMPTY WED-STATE !')
        else:
            plpy.info('JOB NOT FOUND.')
    else:
        plpy.info('JOB WAS ALREADY LOCKED BY ANOTHER PROCESS.')

$body$
language plpython3u;
