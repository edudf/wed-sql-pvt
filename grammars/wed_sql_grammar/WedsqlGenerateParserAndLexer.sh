#!/bin/bash

alias antlr4='java -jar /usr/local/lib/antlr-4.7.1-complete.jar'

antlr4 -Dlanguage=Python3 Wedsql.g4 WedsqlLexer.g4 
mv -f WedsqlLexer.py ../../compilers/wedsql_compiler/WedsqlLexer.py
mv -f Wedsql.py ../../compilers/wedsql_compiler/Wedsql.py
mv -f WedsqlListener.py ../../compilers/wedsql_compiler/WedsqlListener.py
