lexer grammar WedsqlLexer;

WS : [\t\r\n]+->skip;

CREATE : [Cc] [Rr] [Ee] [Aa] [Tt] [Ee];

WED_FLOW : [Ww] [Ee] [Dd] '-' [Ff] [Ll] [Oo] [Ww];

FLOW : [Ff] [Ll] [Oo] [Ww];

REMOVE : [Rr] [Ee] [Mm] [Oo] [Vv] [Ee];

BEGIN : [Bb] [Ee] [Gg] [Ii] [Nn];

COMMIT : [Cc] [Oo] [Mm] [Mm] [Ii] [Tt];

ABORT : [Aa] [Bb] [Oo] [Rr] [Tt];

ROLLBACK : [Rr] [Oo] [Ll] [Ll] [Bb] [Aa] [Cc] [Kk];

CW : '\\' [Cc] [Ww];

LW : '\\' [Ll] [Ww];

XD : '\\' [Xx] [Dd];

XR : '\\' [Xx] [Rr];

OP : '\\' [Oo] [Pp];

WED_ATTRIBUTES : [Ww] [Ee] [Dd] '-' [Aa] [Tt] [Tt] [Rr] [Ii] [Bb] [Uu] [Tt] [Ee] [Ss];

WED_ATTRIBUTE : [Ww] [Ee] [Dd] '-' [Aa] [Tt] [Tt] [Rr] [Ii] [Bb] [Uu] [Tt] [Ee];

WED_CONDITIONS : [Ww] [Ee] [Dd] '-' [Cc] [Oo] [Nn] [Dd] [Ii] [Tt] [Ii] [Oo] [Nn] [Ss];

WED_CONDITION : [Ww] [Ee] [Dd] '-' [Cc] [Oo] [Nn] [Dd] [Ii] [Tt] [Ii] [Oo] [Nn];

WED_TRANSITIONS : [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Aa] [Nn] [Ss] [Ii] [Tt] [Ii] [Oo] [Nn] [Ss];

WED_TRANSITION : [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Aa] [Nn] [Ss] [Ii] [Tt] [Ii] [Oo] [Nn];

WED_TRIGGERS : [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Ii] [Gg] [Gg] [Ee] [Rr] [Ss];

WED_TRIGGER : [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Ii] [Gg] [Gg] [Ee] [Rr];

INSTANCES : [Ii] [Nn] [Ss] [Tt] [Aa] [Nn] [Cc] [Ee] [Ss];

INSTANCE : [Ii] [Nn] [Ss] [Tt] [Aa] [Nn] [Cc] [Ee];

INCONSISTENT : [Ii] [Nn] [Cc] [Oo] [Nn] [Ss] [Ii] [Ss] [Tt] [Ee] [Nn] [Tt];

TRANSACTIONING : [Tt] [Rr] [Aa] [Nn] [Ss] [Aa] [Cc] [Tt] [Ii] [Oo] [Nn] [Ii] [Nn] [Gg];

FINAL : [Ff] [Ii] [Nn] [Aa] [Ll];

SET : [Ss] [Ee] [Tt];

WED_STATE : [Ww] [Ee] [Dd] '-' [Ss] [Tt] [Aa] [Tt] [Ee];

ENABLE : [Ee] [Nn] [Aa] [Bb] [Ll] [Ee];

DISABLE : [Dd] [Ii] [Ss] [Aa] [Bb] [Ll] [Ee];

LIST : [Ll] [Ii] [Ss] [Tt];

FIRED : [Ff] [Ii] [Rr] [Ee] [Dd];

GET : [Gg] [Ee] [Tt];

WED_TRACE : [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Aa] [Cc] [Ee];

SHOW : [Ss] [Hh] [Oo] [Ww];

DEFAULT : [Dd] [Ee] [Ff] [Aa] [Uu] [Ll] [Tt];

VALUES : [Vv] [Aa] [Ll] [Uu] [Ee] [Ss];

VALUE : [Vv] [Aa] [Ll] [Uu] [Ee];

CONDITION : [Cc] [Oo] [Nn] [Dd] [Ii] [Tt] [Ii] [Oo] [Nn];

TIMEOUT : [Tt] [Ii] [Mm] [Ee] [Oo] [Uu] [Tt];

STOP : [Ss] [Tt] [Oo] [Pp];

ON : [Oo] [Nn];

TERMINATE : [Tt] [Ee] [Rr] [Mm] [Ii] [Nn] [Aa] [Tt] [Ee];

REINSTATE : [Rr] [Ee] [Ii] [Nn] [Ss] [Tt] [Aa] [Tt] [Ee];

AS : [Aa] [Ss];

FOR : [Ff] [Oo] [Rr];

SP : [Ss] [Pp];

SEMICOLON : ';';

COMMA : ',';

OPEN_PARENTESIS : '(';

CLOSE_PARENTESIS : ')';

LINE_COMMENT : ('//'|'--') .*? SEMICOLON;

fragment DOUBLE_QUOTES : '"';

fragment SINGLE_QUOTES : '\'';

QUOTED_VALUE : DOUBLE_QUOTES .*? DOUBLE_QUOTES | SINGLE_QUOTES .*? SINGLE_QUOTES;

NON_SPECIAL : [a-zA-Z] | [\-_];

NAT_NUMBER : DIGIT+;

fragment DIGIT : [0-9];

fragment VALID_NAME : (NON_SPECIAL | NAT_NUMBER | CREATE | WED_FLOW | FLOW | REMOVE | BEGIN | COMMIT | ABORT | ROLLBACK | WED_ATTRIBUTE | WED_CONDITION | WED_TRANSITION | WED_TRIGGER | WED_ATTRIBUTES | WED_CONDITIONS | WED_TRANSITIONS | WED_TRIGGERS | INSTANCES | INSTANCE | INCONSISTENT | TRANSACTIONING | FINAL | SET | WED_STATE | ENABLE | DISABLE | LIST | FIRED | GET | WED_TRACE | SHOW | DEFAULT | VALUES | VALUE | CONDITION | TIMEOUT | STOP | ON | AS | FOR | SP | TERMINATE | REINSTATE )+;

SET_FINAL_STATE_EXPR : FINAL (SPACE|WS)+ WED_STATE (SPACE|WS)+ AS (SPACE|WS)+ .+? SEMICOLON;

CONDITION_EXPR : WED_CONDITION (SPACE|WS)+ VALID_NAME (SPACE|WS)+ AS (SPACE|WS)+ .+? SEMICOLON;

SPACE : ' ';




